public class Debug {
    public static void main(String[] args) {
        for (int block = 0; block < 4096; block++) {
            int dataIndex = block << 1;
            System.out.println(block + " " + dataIndex);
        }
    }
}
