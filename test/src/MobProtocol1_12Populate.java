import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import java.io.File;

public class MobProtocol1_12Populate {

    public static void main(String[] args) throws Exception {
        String jsonEntities = IOUtils.toString(MobProtocol1_12Populate.class.getResourceAsStream("entities1.13.json"));
        String jsonCurrent = IOUtils.toString(MobProtocol1_12Populate.class.getResourceAsStream("mob-protocol1.12.2.json"));

        Gson gson = new Gson();
        JsonElement rootCurrent = gson.fromJson(jsonCurrent, JsonElement.class);
        JsonElement rootEntities = gson.fromJson(jsonEntities, JsonElement.class);

        for (JsonElement item : rootCurrent.getAsJsonArray()) {
            JsonObject itemObject = item.getAsJsonObject();
            String anEnum = itemObject.get("enum").getAsString();
            for (JsonElement map : itemObject.get("map").getAsJsonArray()) {
                JsonObject object = map.getAsJsonObject();
                if (object.get("version").getAsString().equals("MINECRAFT_1_13")) {
                    for (JsonElement element : rootEntities.getAsJsonArray()) {
                        String itemName = element.getAsJsonObject().get("name").getAsString();
                        if (itemName.equalsIgnoreCase(anEnum)) {
                            int id = element.getAsJsonObject().get("id").getAsInt();
                            object.remove("id");
                            object.remove("version");
                            object.addProperty("id", id);
                            object.addProperty("version", "MINECRAFT_1_13");
                        }
                    }
                }
            }
        }

        String json = gson.toJson(rootCurrent);

        FileUtils.writeStringToFile(new File("out.txt"), json);
    }
}
