import com.google.gson.Gson;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PopulateBlockProtocol {
    public static void main(String[] args) throws Exception {
        Gson gson = new Gson();
        List list = gson.fromJson(FileUtils.readFileToString(new File("blocks1.13.json")), List.class);

        String currentProtocol = FileUtils.readFileToString(new File("current-protocol.txt")).replace("\r", "");

        Map<String, String> populate = new HashMap<>();
        for (Object o : list) {
            Map map = (Map) o;
            int minStateId = ((Number) map.get("minStateId")).intValue();
            String name = (String) map.get("name");
            populate.put(name, String.valueOf(minStateId));
        }

        String[] current = currentProtocol.split("\n");
        for (int i = 0; i < current.length; i++) {
            String line = current[i];

            String materialName = StringUtils.substringBetween(line, ".register(Material.", ", map").toLowerCase();
            String newId = populate.get(materialName);
            if (newId != null) {
                current[i] = line.replace("))", "), map(Version.MINECRAFT_1_13, " + newId + "))");
            } else {
                current[i] = line.replace("))", "), map(Version.MINECRAFT_1_13, -1))");
//                System.out.println("not found: " + materialName);
            }
            System.out.println(current[i]);
        }
    }
}
