package ua.lokha.multiprotocol;

import io.netty.channel.Channel;

/**
 * Сюда вынесенен весь включевой функционал плагина
 */
public class MultiProtocolApi {

    public static Connection getConnection(Channel channel) {
        return Connection.get(channel);
    }

    public static HandlerManager getHandlerManager() {
        return HandlerManager.getInstance();
    }

    public static RemapInboundHandler getRemapInboundHandler() {
        return InjectChannelInitializer.getRemapInboundHandler();
    }

    public static RemapOutboundHandler getRemapOutboundHandler() {
        return InjectChannelInitializer.getRemapOutboundHandler();
    }
}
