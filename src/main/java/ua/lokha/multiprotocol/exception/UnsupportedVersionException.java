package ua.lokha.multiprotocol.exception;

import ua.lokha.multiprotocol.Version;

/**
 * Кидается, когда версия не поддерживается
 */
public class UnsupportedVersionException extends RuntimeException {
    public UnsupportedVersionException(Version version) {
        super(String.valueOf(version));
    }

    public UnsupportedVersionException(Version version, String message) {
        super(version + ": " + message);
    }

    public UnsupportedVersionException(String message) {
        super(message);
    }
}
