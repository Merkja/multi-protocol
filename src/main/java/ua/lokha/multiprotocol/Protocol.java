package ua.lokha.multiprotocol;

import ua.lokha.multiprotocol.packet.handshake.HandshakePacket;
import ua.lokha.multiprotocol.packet.login.*;
import ua.lokha.multiprotocol.packet.play.*;
import ua.lokha.multiprotocol.packet.status.InPingPacket;
import ua.lokha.multiprotocol.packet.status.OutPingPacket;
import ua.lokha.multiprotocol.packet.status.StatusRequestPacket;
import ua.lokha.multiprotocol.packet.status.StatusResponsePacket;

import java.util.Arrays;
import java.util.List;

import static ua.lokha.multiprotocol.Id2TypeVersionMapper.map;

/**
 * Типы протоколов
 */
public enum Protocol {
    HANDSHAKE {{
        TO_SERVER.register(HandshakePacket.class, HandshakePacket::new, map(Version.MINECRAFT_1_8, 0x00));
    }},
    STATUS{{
        TO_CLIENT.register(StatusResponsePacket.class, StatusResponsePacket::new, map(Version.MINECRAFT_1_8, 0x00));
        TO_CLIENT.register(OutPingPacket.class, OutPingPacket::new, map(Version.MINECRAFT_1_8, 0x01));
        TO_SERVER.register(StatusRequestPacket.class, StatusRequestPacket::new, map( Version.MINECRAFT_1_8, 0x00 ));
        TO_SERVER.register(InPingPacket.class, InPingPacket::new, map( Version.MINECRAFT_1_8, 0x01 ));
    }},
    LOGIN{{
        TO_CLIENT.register(LoginKickPacket.class, LoginKickPacket::new, map(Version.MINECRAFT_1_8, 0x00));
        TO_CLIENT.register(EncryptionRequestPacket.class, EncryptionRequestPacket::new, map(Version.MINECRAFT_1_8, 0x01));
        TO_CLIENT.register(LoginSuccessPacket.class, LoginSuccessPacket::new, map(Version.MINECRAFT_1_8, 0x02));
        TO_CLIENT.register(SetCompressionPacket.class, SetCompressionPacket::new, map(Version.MINECRAFT_1_8, 0x03));
        TO_CLIENT.register(LoginPayloadRequestPacket.class, LoginPayloadRequestPacket::new, map(Version.MINECRAFT_1_13, 0x04));
        TO_SERVER.register(LoginRequestPacket.class, LoginRequestPacket::new, map(Version.MINECRAFT_1_8, 0x00));
        TO_SERVER.register(EncryptionResponsePacket.class, EncryptionResponsePacket::new, map(Version.MINECRAFT_1_8, 0x01));
        TO_SERVER.register(LoginPayloadResponsePacket.class, LoginPayloadResponsePacket::new, map(Version.MINECRAFT_1_13, 0x02));
    }},
    PLAY{{
        this.TO_CLIENT.register(OutKeepAlivePacket.class, OutKeepAlivePacket::new,
                map( Version.MINECRAFT_1_8, 0x00 ),
                map( Version.MINECRAFT_1_9, 0x1F ),
                map( Version.MINECRAFT_1_13, 0x21 ),
                map( Version.MINECRAFT_1_14, 0x20 ));
        this.TO_CLIENT.register(LoginPacket.class, LoginPacket::new,
                map( Version.MINECRAFT_1_8, 0x01 ),
                map( Version.MINECRAFT_1_9, 0x23 ),
                map( Version.MINECRAFT_1_13, 0x25 ));
        this.TO_CLIENT.register(OutChatPacket.class, OutChatPacket::new,
                map( Version.MINECRAFT_1_8, 0x02 ),
                map( Version.MINECRAFT_1_9, 0x0F ),
                map( Version.MINECRAFT_1_13, 0x0E ));
        this.TO_CLIENT.register(RespawnPacket.class, RespawnPacket::new,
                map( Version.MINECRAFT_1_8, 0x07 ),
                map( Version.MINECRAFT_1_9, 0x33 ),
                map( Version.MINECRAFT_1_12, 0x34 ),
                map( Version.MINECRAFT_1_12_1, 0x35 ),
                map( Version.MINECRAFT_1_13, 0x38 ),
                map( Version.MINECRAFT_1_14, 0x3A ));
        this.TO_CLIENT.register(BossBarPacket.class, BossBarPacket::new, map( Version.MINECRAFT_1_9, 0x0C ));
        this.TO_CLIENT.register(PlayerListItemPacket.class, PlayerListItemPacket::new,
                map( Version.MINECRAFT_1_8, 0x38 ),
                map( Version.MINECRAFT_1_9, 0x2D ),
                map( Version.MINECRAFT_1_12_1, 0x2E ),
                map( Version.MINECRAFT_1_13, 0x30 ),
                map( Version.MINECRAFT_1_14, 0x33 ));
        this.TO_CLIENT.register(TabCompleteResponsePacket.class, TabCompleteResponsePacket::new,
                map( Version.MINECRAFT_1_8, 0x3A ),
                map( Version.MINECRAFT_1_9, 0x0E ),
                map( Version.MINECRAFT_1_13, 0x10 ));
        this.TO_CLIENT.register(ScoreboardObjectivePacket.class, ScoreboardObjectivePacket::new,
                map( Version.MINECRAFT_1_8, 0x3B ),
                map( Version.MINECRAFT_1_9, 0x3F ),
                map( Version.MINECRAFT_1_12, 0x41 ),
                map( Version.MINECRAFT_1_12_1, 0x42 ),
                map( Version.MINECRAFT_1_13, 0x45 ),
                map( Version.MINECRAFT_1_14, 0x49 ));
        this.TO_CLIENT.register(ScoreboardScorePacket.class, ScoreboardScorePacket::new,
                map( Version.MINECRAFT_1_8, 0x3C ),
                map( Version.MINECRAFT_1_9, 0x42 ),
                map( Version.MINECRAFT_1_12, 0x44 ),
                map( Version.MINECRAFT_1_12_1, 0x45 ),
                map( Version.MINECRAFT_1_13, 0x48 ),
                map( Version.MINECRAFT_1_14, 0x4C ));
        this.TO_CLIENT.register(ScoreboardDisplayPacket.class, ScoreboardDisplayPacket::new,
                map( Version.MINECRAFT_1_8, 0x3D ),
                map( Version.MINECRAFT_1_9, 0x38 ),
                map( Version.MINECRAFT_1_12, 0x3A ),
                map( Version.MINECRAFT_1_12_1, 0x3B ),
                map( Version.MINECRAFT_1_13, 0x3E ),
                map( Version.MINECRAFT_1_14, 0x42 ));
        this.TO_CLIENT.register(TeamPacket.class, TeamPacket::new,
                map( Version.MINECRAFT_1_8, 0x3E ),
                map( Version.MINECRAFT_1_9, 0x41 ),
                map( Version.MINECRAFT_1_12, 0x43 ),
                map( Version.MINECRAFT_1_12_1, 0x44 ),
                map( Version.MINECRAFT_1_13, 0x47 ),
                map( Version.MINECRAFT_1_14, 0x4B ));
        // todo кикает 1.13
        /*this.TO_CLIENT.registerPacket(OutPluginMessagePacket.class, OutPluginMessagePacket::new,
                map( Version.MINECRAFT_1_8, 0x3F ),
                map( Version.MINECRAFT_1_9, 0x18 ),
                map( Version.MINECRAFT_1_13, 0x19 ),
                map( Version.MINECRAFT_1_14, 0x18 ));*/
        this.TO_CLIENT.register(KickPacket.class, KickPacket::new,
                map( Version.MINECRAFT_1_8, 0x40 ),
                map( Version.MINECRAFT_1_9, 0x1A ),
                map( Version.MINECRAFT_1_13, 0x1B ),
                map( Version.MINECRAFT_1_14, 0x1A ));
        this.TO_CLIENT.register(TitlePacket.class, TitlePacket::new,
                map( Version.MINECRAFT_1_8, 0x45 ),
                map( Version.MINECRAFT_1_12, 0x47 ),
                map( Version.MINECRAFT_1_12_1, 0x48 ),
                map( Version.MINECRAFT_1_13, 0x4B ),
                map( Version.MINECRAFT_1_14, 0x4F ));
        this.TO_CLIENT.register(PlayerListHeaderFooterPacket.class, PlayerListHeaderFooterPacket::new,
                map( Version.MINECRAFT_1_8, 0x47 ),
                map( Version.MINECRAFT_1_9, 0x46 ),
                map( Version.MINECRAFT_1_10, 0x47 ),
                map( Version.MINECRAFT_1_12, 0x49 ),
                map( Version.MINECRAFT_1_12_1, 0x4A ),
                map( Version.MINECRAFT_1_13, 0x4E ),
                map( Version.MINECRAFT_1_14, 0x53 ));
        this.TO_CLIENT.register(EntityStatusPacket.class, EntityStatusPacket::new,
                map( Version.MINECRAFT_1_8, 0x1A ),
                map( Version.MINECRAFT_1_9, 0x1B ),
                map( Version.MINECRAFT_1_13, 0x1C ),
                map( Version.MINECRAFT_1_14, 0x1B ));
        this.TO_CLIENT.register(CommandsPacket.class, CommandsPacket::new, map( Version.MINECRAFT_1_13, 0x11 ));
        this.TO_CLIENT.register(UpdateViewDistancePacket.class, UpdateViewDistancePacket::new, map( Version.MINECRAFT_1_14, 0x41 ));
        this.TO_SERVER.register(InKeepAlivePacket.class, InKeepAlivePacket::new,
                map( Version.MINECRAFT_1_8, 0x00 ),
                map( Version.MINECRAFT_1_9, 0x0B ),
                map( Version.MINECRAFT_1_12, 0x0C ),
                map( Version.MINECRAFT_1_12_1, 0x0B ),
                map( Version.MINECRAFT_1_13, 0x0E ),
                map( Version.MINECRAFT_1_14, 0x0F ));
        this.TO_SERVER.register(InChatPacket.class, InChatPacket::new,
                map( Version.MINECRAFT_1_8, 0x01 ),
                map( Version.MINECRAFT_1_9, 0x02 ),
                map( Version.MINECRAFT_1_12, 0x03 ),
                map( Version.MINECRAFT_1_12_1, 0x02 ),
                map( Version.MINECRAFT_1_14, 0x03 ));
        this.TO_SERVER.register(TabCompleteRequestPacket.class, TabCompleteRequestPacket::new,
                map( Version.MINECRAFT_1_8, 0x14 ),
                map( Version.MINECRAFT_1_9, 0x01 ),
                map( Version.MINECRAFT_1_12, 0x02 ),
                map( Version.MINECRAFT_1_12_1, 0x01 ),
                map( Version.MINECRAFT_1_13, 0x05 ),
                map( Version.MINECRAFT_1_14, 0x06 ));
        this.TO_SERVER.register(ClientSettingsPacket.class, ClientSettingsPacket::new,
                map( Version.MINECRAFT_1_8, 0x15 ),
                map( Version.MINECRAFT_1_9, 0x04 ),
                map( Version.MINECRAFT_1_12, 0x05 ),
                map( Version.MINECRAFT_1_12_1, 0x04 ),
                map( Version.MINECRAFT_1_14, 0x05 ));
        this.TO_SERVER.register(InPluginMessagePacket.class, InPluginMessagePacket::new,
                map( Version.MINECRAFT_1_8, 0x17 ),
                map( Version.MINECRAFT_1_9, 0x09 ),
                map( Version.MINECRAFT_1_12, 0x0A ),
                map( Version.MINECRAFT_1_12_1, 0x09 ),
                map( Version.MINECRAFT_1_13, 0x0A ),
                map( Version.MINECRAFT_1_14, 0x0B ));
        this.TO_CLIENT.register(CombatEventPacket.class, CombatEventPacket::new,
                map(Version.MINECRAFT_1_8, 0x42),
                map(Version.MINECRAFT_1_9, 0x2C),
                map(Version.MINECRAFT_1_12_1, 0x2D),
                map(Version.MINECRAFT_1_13, 0x2F),
                map(Version.MINECRAFT_1_14, 0x32));
        this.TO_CLIENT.register(OutPlayerAbilitiesPacket.class, OutPlayerAbilitiesPacket::new,
                map(Version.MINECRAFT_1_8, 0x39),
                map(Version.MINECRAFT_1_9, 0x2B),
                map(Version.MINECRAFT_1_12_1, 0x2C),
                map(Version.MINECRAFT_1_13, 0x2E),
                map(Version.MINECRAFT_1_14, 0x31));
        this.TO_CLIENT.register(ServerDifficultyPacket.class, ServerDifficultyPacket::new,
                map(Version.MINECRAFT_1_8, 0x41),
                map(Version.MINECRAFT_1_9, 0x0D));
        this.TO_CLIENT.register(OutHeldItemChangePacket.class, OutHeldItemChangePacket::new,
                map(Version.MINECRAFT_1_8, 0x09),
                map(Version.MINECRAFT_1_9, 0x37),
                map(Version.MINECRAFT_1_12, 0x39),
                map(Version.MINECRAFT_1_12_1, 0x3A),
                map(Version.MINECRAFT_1_13, 0x3D),
                map(Version.MINECRAFT_1_14, 0x3F));
        // todo кикает 1.13
        /*this.TO_CLIENT.register(UnlockRecipesPacket.class, UnlockRecipesPacket::new,
                map(Version.MINECRAFT_1_12, 0x30),
                map(Version.MINECRAFT_1_12_1, 0x31),
                map(Version.MINECRAFT_1_13, 0x34),
                map(Version.MINECRAFT_1_14, 0x36));*/
       this.TO_CLIENT.register(EntityPropertiesPacket.class, EntityPropertiesPacket::new,
                map(Version.MINECRAFT_1_8, 0x20),
                map(Version.MINECRAFT_1_9, 0x4B),
                map(Version.MINECRAFT_1_10, 0x4A),
                map(Version.MINECRAFT_1_12, 0x4D),
                map(Version.MINECRAFT_1_12_1, 0x4E),
                map(Version.MINECRAFT_1_13, 0x52),
                map(Version.MINECRAFT_1_14, 0x58));
        this.TO_CLIENT.register(EntityHeadLookPacket.class, EntityHeadLookPacket::new,
                map(Version.MINECRAFT_1_8, 0x19),
                map(Version.MINECRAFT_1_9, 0x34),
                map(Version.MINECRAFT_1_12, 0x35),
                map(Version.MINECRAFT_1_12_1, 0x36),
                map(Version.MINECRAFT_1_13, 0x39),
                map(Version.MINECRAFT_1_14, 0x3B));
        this.TO_CLIENT.register(TimeUpdatePacket.class, TimeUpdatePacket::new,
                map(Version.MINECRAFT_1_8, 0x03),
                map(Version.MINECRAFT_1_9, 0x44),
                map(Version.MINECRAFT_1_12, 0x46),
                map(Version.MINECRAFT_1_12_1, 0x47),
                map(Version.MINECRAFT_1_13, 0x4A),
                map(Version.MINECRAFT_1_14, 0x4E));
        this.TO_CLIENT.register(OutPlayerPositionAndLookPacket.class, OutPlayerPositionAndLookPacket::new,
                map(Version.MINECRAFT_1_8, 0x08),
                map(Version.MINECRAFT_1_9, 0x2E),
                map(Version.MINECRAFT_1_12_1, 0x2F),
                map(Version.MINECRAFT_1_13, 0x32),
                map(Version.MINECRAFT_1_14, 0x35));
        this.TO_CLIENT.register(WorldBorderPacket.class, WorldBorderPacket::new,
                map(Version.MINECRAFT_1_8, 0x44),
                map(Version.MINECRAFT_1_9, 0x35),
                map(Version.MINECRAFT_1_12, 0x37),
                map(Version.MINECRAFT_1_12_1, 0x38),
                map(Version.MINECRAFT_1_13, 0x3B),
                map(Version.MINECRAFT_1_14, 0x3D));
        this.TO_CLIENT.register(EntityTeleportPacket.class, EntityTeleportPacket::new,
                map(Version.MINECRAFT_1_8, 0x18),
                map(Version.MINECRAFT_1_9, 0x4A),
                map(Version.MINECRAFT_1_10, 0x49),
                map(Version.MINECRAFT_1_12, 0x4B),
                map(Version.MINECRAFT_1_12_1, 0x4C),
                map(Version.MINECRAFT_1_13, 0x50),
                map(Version.MINECRAFT_1_14, 0x56));
        this.TO_CLIENT.register(EntityLookAndRelativeMovePacket.class, EntityLookAndRelativeMovePacket::new,
                map(Version.MINECRAFT_1_8, 0x17),
                map(Version.MINECRAFT_1_9, 0x26),
                map(Version.MINECRAFT_1_12, 0x27),
                map(Version.MINECRAFT_1_13, 0x29));
        this.TO_CLIENT.register(EntityRelativeMovePacket.class, EntityRelativeMovePacket::new,
                map(Version.MINECRAFT_1_8, 0x15),
                map(Version.MINECRAFT_1_9, 0x25),
                map(Version.MINECRAFT_1_12, 0x26),
                map(Version.MINECRAFT_1_13, 0x28));
        this.TO_CLIENT.register(EntityLookPacket.class, EntityLookPacket::new,
                map(Version.MINECRAFT_1_8, 0x16),
                map(Version.MINECRAFT_1_9, 0x27),
                map(Version.MINECRAFT_1_12, 0x28),
                map(Version.MINECRAFT_1_13, 0x2A));
        this.TO_CLIENT.register(EntityPacket.class, EntityPacket::new,
                map(Version.MINECRAFT_1_8, 0x14),
                map(Version.MINECRAFT_1_9, 0x28),
                map(Version.MINECRAFT_1_12, 0x25),
                map(Version.MINECRAFT_1_13, 0x27),
                map(Version.MINECRAFT_1_14, 0x2B));
        this.TO_CLIENT.register(SpawnPositionPacket.class, SpawnPositionPacket::new,
                map(Version.MINECRAFT_1_8, 0x05),
                map(Version.MINECRAFT_1_9, 0x43),
                map(Version.MINECRAFT_1_12, 0x45),
                map(Version.MINECRAFT_1_12_1, 0x46),
                map(Version.MINECRAFT_1_13, 0x49),
                map(Version.MINECRAFT_1_14, 0x4D));
        this.TO_CLIENT.register(BlockChangePacket.class, BlockChangePacket::new,
                map(Version.MINECRAFT_1_8, 0x23),
                map(Version.MINECRAFT_1_9, 0x0B));
        this.TO_CLIENT.register(UpdateHealthPacket.class, UpdateHealthPacket::new,
                map(Version.MINECRAFT_1_8, 0x06),
                map(Version.MINECRAFT_1_9, 0x3E),
                map(Version.MINECRAFT_1_12, 0x40),
                map(Version.MINECRAFT_1_12_1, 0x41),
                map(Version.MINECRAFT_1_13, 0x44),
                map(Version.MINECRAFT_1_14, 0x48));
        this.TO_CLIENT.register(ChunkDataPacket.class, ChunkDataPacket::new,
                map(Version.MINECRAFT_1_8, 0x21),
                map(Version.MINECRAFT_1_9, 0x20),
                map(Version.MINECRAFT_1_13, 0x22),
                map(Version.MINECRAFT_1_14, 0x21));
        this.TO_CLIENT.register(SpawnObjectPacket.class, SpawnObjectPacket::new,
                map(Version.MINECRAFT_1_8, 0x0E),
                map(Version.MINECRAFT_1_9, 0x00));
        this.TO_CLIENT.register(SpawnExperienceOrbPacket.class, SpawnExperienceOrbPacket::new,
                map(Version.MINECRAFT_1_8, 0x11),
                map(Version.MINECRAFT_1_9, 0x01));
        this.TO_CLIENT.register(SpawnPlayerPacket.class, SpawnPlayerPacket::new,
                map(Version.MINECRAFT_1_8, 0x0C),
                map(Version.MINECRAFT_1_9, 0x05));
        this.TO_CLIENT.register(SpawnMobPacket.class, SpawnMobPacket::new,
                map(Version.MINECRAFT_1_8, 0x0F),
                map(Version.MINECRAFT_1_9, 0x03));
        this.TO_CLIENT.register(SpawnPaintingPacket.class, SpawnPaintingPacket::new,
                map(Version.MINECRAFT_1_8, 0x10),
                map(Version.MINECRAFT_1_9, 0x04));
        this.TO_CLIENT.register(EntityMetadataPacket.class, EntityMetadataPacket::new,
                map(Version.MINECRAFT_1_8, 0x1C),
                map(Version.MINECRAFT_1_9, 0x39),
                map(Version.MINECRAFT_1_12, 0x3B),
                map(Version.MINECRAFT_1_12_1, 0x3C),
                map(Version.MINECRAFT_1_13, 0x3F),
                map(Version.MINECRAFT_1_14, 0x43));
        this.TO_CLIENT.register(EntityVelocityPacket.class, EntityVelocityPacket::new,
                map(Version.MINECRAFT_1_8, 0x12),
                map(Version.MINECRAFT_1_9, 0x3B),
                map(Version.MINECRAFT_1_12, 0x3D),
                map(Version.MINECRAFT_1_12_1, 0x3E),
                map(Version.MINECRAFT_1_13, 0x41),
                map(Version.MINECRAFT_1_14, 0x45));
        this.TO_SERVER.register(UseEntityPacket.class, UseEntityPacket::new,
                map(Version.MINECRAFT_1_8, 0x02),
                map(Version.MINECRAFT_1_9, 0x0A),
                map(Version.MINECRAFT_1_12, 0x0B),
                map(Version.MINECRAFT_1_12_1, 0x0A),
                map(Version.MINECRAFT_1_13, 0x0D),
                map(Version.MINECRAFT_1_14, 0x0E));
        this.TO_SERVER.register(PlayerPacket.class, PlayerPacket::new,
                map(Version.MINECRAFT_1_8, 0x03),
                map(Version.MINECRAFT_1_9, 0x0F),
                map(Version.MINECRAFT_1_12, 0x0D),
                map(Version.MINECRAFT_1_12_1, 0x0C),
                map(Version.MINECRAFT_1_13, 0x0F),
                map(Version.MINECRAFT_1_14, 0x14));
        this.TO_SERVER.register(PlayerPositionPacket.class, PlayerPositionPacket::new,
                map(Version.MINECRAFT_1_8, 0x04),
                map(Version.MINECRAFT_1_9, 0x0C),
                map(Version.MINECRAFT_1_12, 0x0E),
                map(Version.MINECRAFT_1_12_1, 0x0D),
                map(Version.MINECRAFT_1_13, 0x10),
                map(Version.MINECRAFT_1_14, 0x11));
        this.TO_SERVER.register(PlayerLookPacket.class, PlayerLookPacket::new,
                map(Version.MINECRAFT_1_8, 0x05),
                map(Version.MINECRAFT_1_9, 0x0E),
                map(Version.MINECRAFT_1_12, 0x10),
                map(Version.MINECRAFT_1_12_1, 0x0F),
                map(Version.MINECRAFT_1_13, 0x12),
                map(Version.MINECRAFT_1_14, 0x13));
        this.TO_SERVER.register(InPlayerPositionAndLookPacket.class, InPlayerPositionAndLookPacket::new,
                map(Version.MINECRAFT_1_8, 0x06),
                map(Version.MINECRAFT_1_9, 0x0D),
                map(Version.MINECRAFT_1_12, 0x0F),
                map(Version.MINECRAFT_1_12_1, 0x0E),
                map(Version.MINECRAFT_1_13, 0x11),
                map(Version.MINECRAFT_1_14, 0x12));
        this.TO_CLIENT.register(ChangeGameStatePacket.class, ChangeGameStatePacket::new,
                map(Version.MINECRAFT_1_8, 0x2B),
                map(Version.MINECRAFT_1_9, 0x1E),
                map(Version.MINECRAFT_1_13, 0x20),
                map(Version.MINECRAFT_1_14, 0x1E));
        this.TO_CLIENT.register(SpawnGlobalEntityPacket.class, SpawnGlobalEntityPacket::new,
                map(Version.MINECRAFT_1_8, 0x2C),
                map(Version.MINECRAFT_1_9, 0x02));
        this.TO_CLIENT.register(WindowItemsPacket.class, WindowItemsPacket::new,
                map(Version.MINECRAFT_1_8, 0x30),
                map(Version.MINECRAFT_1_9, 0x14),
                map(Version.MINECRAFT_1_13, 0x15),
                map(Version.MINECRAFT_1_14, 0x14));
        this.TO_CLIENT.register(SetSlotPacket.class, SetSlotPacket::new,
                map(Version.MINECRAFT_1_8, 0x2F),
                map(Version.MINECRAFT_1_9, 0x16),
                map(Version.MINECRAFT_1_13, 0x17),
                map(Version.MINECRAFT_1_14, 0x16));
        // todo кикает 1.13
        /*this.TO_CLIENT.register(AdvancementsPacket.class, AdvancementsPacket::new,
                map(Version.MINECRAFT_1_12, 0x4C),
                map(Version.MINECRAFT_1_12_1, 0x4D),
                map(Version.MINECRAFT_1_13, 0x51),
                map(Version.MINECRAFT_1_14, 0x57));*/
        this.TO_SERVER.register(TeleportConfirmPacket.class, TeleportConfirmPacket::new,
                map(Version.MINECRAFT_1_9, 0x00));
        this.TO_CLIENT.register(SoundEffectPacket.class, SoundEffectPacket::new,
                map(Version.MINECRAFT_1_8, 0x29),
                map(Version.MINECRAFT_1_9, 0x47),
                map(Version.MINECRAFT_1_10, 0x46),
                map(Version.MINECRAFT_1_12, 0x48),
                map(Version.MINECRAFT_1_12_1, 0x49),
                map(Version.MINECRAFT_1_13, 0x4D),
                map(Version.MINECRAFT_1_14, 0x51));
        this.TO_CLIENT.register(DestroyEntitiesPacket.class, DestroyEntitiesPacket::new,
                map(Version.MINECRAFT_1_8, 0x13),
                map(Version.MINECRAFT_1_9, 0x30),
                map(Version.MINECRAFT_1_12, 0x31),
                map(Version.MINECRAFT_1_12_1, 0x32),
                map(Version.MINECRAFT_1_13, 0x35),
                map(Version.MINECRAFT_1_14, 0x37));
        this.TO_CLIENT.register(UnloadChunkPacket.class, UnloadChunkPacket::new,
                map(Version.MINECRAFT_1_9, 0x1D),
                map(Version.MINECRAFT_1_13, 0x1F),
                map(Version.MINECRAFT_1_14, 0x1D));
        this.TO_SERVER.register(EntityActionPacket.class, EntityActionPacket::new,
                map(Version.MINECRAFT_1_8, 0x0B),
                map(Version.MINECRAFT_1_9, 0x14),
                map(Version.MINECRAFT_1_12, 0x15),
                map(Version.MINECRAFT_1_13, 0x19),
                map(Version.MINECRAFT_1_14, 0x1B));
        // todo кикает в 1.8
        this.TO_CLIENT.register(EntityEquipmentPacket.class, EntityEquipmentPacket::new,
                map(Version.MINECRAFT_1_8, 0x04),
                map(Version.MINECRAFT_1_9, 0x3C),
                map(Version.MINECRAFT_1_12, 0x3E),
                map(Version.MINECRAFT_1_12_1, 0x3F),
                map(Version.MINECRAFT_1_13, 0x42),
                map(Version.MINECRAFT_1_14, 0x46));
        this.TO_CLIENT.register(MultiBlockChangePacket.class, MultiBlockChangePacket::new,
                map(Version.MINECRAFT_1_8, 0x22),
                map(Version.MINECRAFT_1_9, 0x10),
                map(Version.MINECRAFT_1_13, 0x0F));
        this.TO_CLIENT.register(SetExperiencePacket.class, SetExperiencePacket::new,
                map(Version.MINECRAFT_1_8, 0x1F),
                map(Version.MINECRAFT_1_9, 0x3D),
                map(Version.MINECRAFT_1_12, 0x3F),
                map(Version.MINECRAFT_1_12_1, 0x40),
                map(Version.MINECRAFT_1_13, 0x43),
                map(Version.MINECRAFT_1_14, 0x47));
        this.TO_SERVER.register(InHeldItemChangePacket.class, InHeldItemChangePacket::new,
                map(Version.MINECRAFT_1_8, 0x09),
                map(Version.MINECRAFT_1_9, 0x17),
                map(Version.MINECRAFT_1_12, 0x1A),
                map(Version.MINECRAFT_1_13, 0x21),
                map(Version.MINECRAFT_1_14, 0x23));
        this.TO_SERVER.register(InAnimationPacket.class, InAnimationPacket::new,
                map(Version.MINECRAFT_1_8, 0x0A),
                map(Version.MINECRAFT_1_9, 0x1A),
                map(Version.MINECRAFT_1_12, 0x1D),
                map(Version.MINECRAFT_1_13, 0x27),
                map(Version.MINECRAFT_1_14, 0x2A));
        this.TO_SERVER.register(EffectPacket.class, EffectPacket::new,
                map(Version.MINECRAFT_1_8, 0x28),
                map(Version.MINECRAFT_1_9, 0x21),
                map(Version.MINECRAFT_1_13, 0x23),
                map(Version.MINECRAFT_1_14, 0x22));
        this.TO_SERVER.register(UseItemPacket.class, UseItemPacket::new,
                map(Version.MINECRAFT_1_9, 0x1D),
                map(Version.MINECRAFT_1_12, 0x20),
                map(Version.MINECRAFT_1_13, 0x2A),
                map(Version.MINECRAFT_1_14, 0x2D));
        this.TO_SERVER.register(ClickWindowPacket.class, ClickWindowPacket::new,
                map(Version.MINECRAFT_1_8, 0x0E),
                map(Version.MINECRAFT_1_9, 0x07),
                map(Version.MINECRAFT_1_12, 0x08),
                map(Version.MINECRAFT_1_12_1, 0x07),
                map(Version.MINECRAFT_1_13, 0x08),
                map(Version.MINECRAFT_1_14, 0x09));
        this.TO_SERVER.register(PlayerDiggingPacket.class, PlayerDiggingPacket::new,
                map(Version.MINECRAFT_1_8, 0x07),
                map(Version.MINECRAFT_1_9, 0x13),
                map(Version.MINECRAFT_1_12, 0x14),
                map(Version.MINECRAFT_1_13, 0x18),
                map(Version.MINECRAFT_1_14, 0x1A));
        this.TO_SERVER.register(CreativeInventoryActionPacket.class, CreativeInventoryActionPacket::new,
                map(Version.MINECRAFT_1_8, 0x10),
                map(Version.MINECRAFT_1_9, 0x18),
                map(Version.MINECRAFT_1_12, 0x1B),
                map(Version.MINECRAFT_1_13, 0x24),
                map(Version.MINECRAFT_1_14, 0x26));
        this.TO_SERVER.register(PlayerBlockPlacementPacket.class, PlayerBlockPlacementPacket::new,
                map(Version.MINECRAFT_1_8, 0x08),
                map(Version.MINECRAFT_1_9, 0x1C),
                map(Version.MINECRAFT_1_12, 0x1F),
                map(Version.MINECRAFT_1_13, 0x29),
                map(Version.MINECRAFT_1_14, 0x2C));
        this.TO_CLIENT.register(OpenWindowPacket.class, OpenWindowPacket::new,
                map(Version.MINECRAFT_1_8, 0x2D),
                map(Version.MINECRAFT_1_9, 0x13),
                map(Version.MINECRAFT_1_13, 0x14),
                map(Version.MINECRAFT_1_14, 0x2E));
        this.TO_SERVER.register(ClientStatusPacket.class, ClientStatusPacket::new,
                map(Version.MINECRAFT_1_8, 0x16),
                map(Version.MINECRAFT_1_9, 0x03),
                map(Version.MINECRAFT_1_12, 0x04),
                map(Version.MINECRAFT_1_12_1, 0x03),
                map(Version.MINECRAFT_1_14, 0x04));
        this.TO_SERVER.register(InConfirmTransactionPacket.class, InConfirmTransactionPacket::new,
                map(Version.MINECRAFT_1_8, 0x0F),
                map(Version.MINECRAFT_1_9, 0x05),
                map(Version.MINECRAFT_1_12, 0x06),
                map(Version.MINECRAFT_1_12_1, 0x05),
                map(Version.MINECRAFT_1_13, 0x06),
                map(Version.MINECRAFT_1_14, 0x07));
        this.TO_SERVER.register(EnchantItemPacket.class, EnchantItemPacket::new,
                map(Version.MINECRAFT_1_8, 0x11),
                map(Version.MINECRAFT_1_9, 0x06),
                map(Version.MINECRAFT_1_12, 0x07),
                map(Version.MINECRAFT_1_12_1, 0x06),
                map(Version.MINECRAFT_1_13, 0x07),
                map(Version.MINECRAFT_1_14, 0x08));
        this.TO_SERVER.register(InVehicleMovePacket.class, InVehicleMovePacket::new,
                map(Version.MINECRAFT_1_9, 0x10),
                map(Version.MINECRAFT_1_12, 0x11),
                map(Version.MINECRAFT_1_12_1, 0x10),
                map(Version.MINECRAFT_1_13, 0x13),
                map(Version.MINECRAFT_1_14, 0x15));
        this.TO_SERVER.register(SteerBoatPacket.class, SteerBoatPacket::new,
                map(Version.MINECRAFT_1_9, 0x11),
                map(Version.MINECRAFT_1_12, 0x12),
                map(Version.MINECRAFT_1_12_1, 0x11),
                map(Version.MINECRAFT_1_13, 0x14),
                map(Version.MINECRAFT_1_14, 0x16));
        this.TO_SERVER.register(CraftRecipeRequestPacket.class, CraftRecipeRequestPacket::new,
                map(Version.MINECRAFT_1_12_1, 0x12),
                map(Version.MINECRAFT_1_13, 0x16),
                map(Version.MINECRAFT_1_14, 0x18));
        this.TO_SERVER.register(InPlayerAbilitiesPacket.class, InPlayerAbilitiesPacket::new,
                map(Version.MINECRAFT_1_8, 0x13),
                map(Version.MINECRAFT_1_9, 0x12),
                map(Version.MINECRAFT_1_12, 0x13),
                map(Version.MINECRAFT_1_13, 0x17),
                map(Version.MINECRAFT_1_14, 0x19));
        this.TO_SERVER.register(SteerVehiclePacket.class, SteerVehiclePacket::new,
                map(Version.MINECRAFT_1_8, 0x0C),
                map(Version.MINECRAFT_1_9, 0x15),
                map(Version.MINECRAFT_1_12, 0x16),
                map(Version.MINECRAFT_1_13, 0x1A),
                map(Version.MINECRAFT_1_14, 0x1C));
        this.TO_SERVER.register(CraftingBookDataPacket.class, CraftingBookDataPacket::new,
                map(Version.MINECRAFT_1_12, 0x17),
                map(Version.MINECRAFT_1_13, 0x1B),
                map(Version.MINECRAFT_1_14, 0x1D));
        this.TO_SERVER.register(AdvancementTabPacket.class, AdvancementTabPacket::new,
                map(Version.MINECRAFT_1_12, 0x19),
                map(Version.MINECRAFT_1_13, 0x1E),
                map(Version.MINECRAFT_1_14, 0x20));
        this.TO_SERVER.register(UpdateSignPacket.class, UpdateSignPacket::new,
                map(Version.MINECRAFT_1_12, 0x1C),
                map(Version.MINECRAFT_1_13, 0x26),
                map(Version.MINECRAFT_1_14, 0x29));
        this.TO_SERVER.register(InCloseWindowPacket.class, InCloseWindowPacket::new,
                map(Version.MINECRAFT_1_8, 0x0D),
                map(Version.MINECRAFT_1_9, 0x08),
                map(Version.MINECRAFT_1_12, 0x09),
                map(Version.MINECRAFT_1_12_1, 0x08),
                map(Version.MINECRAFT_1_13, 0x09),
                map(Version.MINECRAFT_1_14, 0x0A));
        this.TO_CLIENT.register(OutCloseWindowPacket.class, OutCloseWindowPacket::new,
                map(Version.MINECRAFT_1_8, 0x2E),
                map(Version.MINECRAFT_1_9, 0x12),
                map(Version.MINECRAFT_1_13, 0x13));
        this.TO_SERVER.register(ResourcePackStatusPacket.class, ResourcePackStatusPacket::new,
                map(Version.MINECRAFT_1_8, 0x19),
                map(Version.MINECRAFT_1_9, 0x16),
                map(Version.MINECRAFT_1_12, 0x18),
                map(Version.MINECRAFT_1_13, 0x1D),
                map(Version.MINECRAFT_1_14, 0x1F));
        this.TO_CLIENT.register(ResourcePackSendPacket.class, ResourcePackSendPacket::new,
                map(Version.MINECRAFT_1_8, 0x48),
                map(Version.MINECRAFT_1_9, 0x32),
                map(Version.MINECRAFT_1_12, 0x33),
                map(Version.MINECRAFT_1_12_1, 0x34),
                map(Version.MINECRAFT_1_13, 0x37),
                map(Version.MINECRAFT_1_14, 0x39));
        this.TO_SERVER.register(SpectatePacket.class, SpectatePacket::new,
                map(Version.MINECRAFT_1_8, 0x18),
                map(Version.MINECRAFT_1_9, 0x1B),
                map(Version.MINECRAFT_1_12, 0x1E),
                map(Version.MINECRAFT_1_13, 0x28),
                map(Version.MINECRAFT_1_14, 0x2B));
        this.TO_CLIENT.register(OutAnimationPacket.class, OutAnimationPacket::new,
                map(Version.MINECRAFT_1_8, 0x0B),
                map(Version.MINECRAFT_1_9, 0x06));
        this.TO_CLIENT.register(StatisticsPacket.class, StatisticsPacket::new,
                map(Version.MINECRAFT_1_8, 0x37),
                map(Version.MINECRAFT_1_9, 0x07));
        this.TO_CLIENT.register(BlockBreakAnimationPacket.class, BlockBreakAnimationPacket::new,
                map(Version.MINECRAFT_1_8, 0x25),
                map(Version.MINECRAFT_1_9, 0x08));
        this.TO_CLIENT.register(UpdateBlockEntityPacket.class, UpdateBlockEntityPacket::new,
                map(Version.MINECRAFT_1_8, 0x35),
                map(Version.MINECRAFT_1_9, 0x09));
        this.TO_CLIENT.register(BlockActionPacket.class, BlockActionPacket::new,
                map(Version.MINECRAFT_1_8, 0x24),
                map(Version.MINECRAFT_1_9, 0x0A));
        this.TO_CLIENT.register(OutConfirmTransactionPacket.class, OutConfirmTransactionPacket::new,
                map(Version.MINECRAFT_1_8, 0x32),
                map(Version.MINECRAFT_1_9, 0x11),
                map(Version.MINECRAFT_1_13, 0x12));
        this.TO_CLIENT.register(WindowPropertyPacket.class, WindowPropertyPacket::new,
                map(Version.MINECRAFT_1_8, 0x31),
                map(Version.MINECRAFT_1_9, 0x15),
                map(Version.MINECRAFT_1_13, 0x16),
                map(Version.MINECRAFT_1_14, 0x15));
        this.TO_CLIENT.register(SetCooldownPacket.class, SetCooldownPacket::new,
                map(Version.MINECRAFT_1_9, 0x17),
                map(Version.MINECRAFT_1_13, 0x18),
                map(Version.MINECRAFT_1_14, 0x17));
        this.TO_CLIENT.register(NamedSoundEffectPacket.class, NamedSoundEffectPacket::new,
                map(Version.MINECRAFT_1_9, 0x19),
                map(Version.MINECRAFT_1_13, 0x1A),
                map(Version.MINECRAFT_1_14, 0x19));
        this.TO_CLIENT.register(ExplosionPacket.class, ExplosionPacket::new,
                map(Version.MINECRAFT_1_8, 0x27),
                map(Version.MINECRAFT_1_9, 0x1C),
                map(Version.MINECRAFT_1_13, 0x1E),
                map(Version.MINECRAFT_1_14, 0x1C));
        this.TO_CLIENT.register(ParticlePacket.class, ParticlePacket::new,
                map(Version.MINECRAFT_1_8, 0x2A),
                map(Version.MINECRAFT_1_9, 0x22),
                map(Version.MINECRAFT_1_13, 0x24),
                map(Version.MINECRAFT_1_14, 0x23));
        this.TO_CLIENT.register(MapPacket.class, MapPacket::new,
                map(Version.MINECRAFT_1_8, 0x34),
                map(Version.MINECRAFT_1_9, 0x24),
                map(Version.MINECRAFT_1_13, 0x26));
        this.TO_CLIENT.register(OutVehicleMovePacket.class, OutVehicleMovePacket::new,
                map(Version.MINECRAFT_1_9, 0x29),
                map(Version.MINECRAFT_1_13, 0x2B),
                map(Version.MINECRAFT_1_14, 0x2C));
        this.TO_CLIENT.register(OpenSignEditorPacket.class, OpenSignEditorPacket::new,
                map(Version.MINECRAFT_1_8, 0x36),
                map(Version.MINECRAFT_1_9, 0x2A),
                map(Version.MINECRAFT_1_13, 0x2C),
                map(Version.MINECRAFT_1_14, 0x2F));
        this.TO_CLIENT.register(CraftRecipeResponsePacket.class, CraftRecipeResponsePacket::new,
                map(Version.MINECRAFT_1_12_1, 0x2B),
                map(Version.MINECRAFT_1_13, 0x2D),
                map(Version.MINECRAFT_1_14, 0x30));
        this.TO_CLIENT.register(UseBedPacket.class, UseBedPacket::new,
                map(Version.MINECRAFT_1_8, 0x0A),
                map(Version.MINECRAFT_1_9, 0x2F),
                map(Version.MINECRAFT_1_12_1, 0x30),
                map(Version.MINECRAFT_1_13, 0x33),
                map(Version.MINECRAFT_1_14, -1));
        this.TO_CLIENT.register(RemoveEntityEffectPacket.class, RemoveEntityEffectPacket::new,
                map(Version.MINECRAFT_1_8, 0x1E),
                map(Version.MINECRAFT_1_9, 0x31),
                map(Version.MINECRAFT_1_12, 0x32),
                map(Version.MINECRAFT_1_12_1, 0x33),
                map(Version.MINECRAFT_1_13, 0x36),
                map(Version.MINECRAFT_1_14, 0x38));
        this.TO_CLIENT.register(SelectAdvancementTabPacket.class, SelectAdvancementTabPacket::new,
                map(Version.MINECRAFT_1_12, 0x36),
                map(Version.MINECRAFT_1_12_1, 0x37),
                map(Version.MINECRAFT_1_13, 0x3A),
                map(Version.MINECRAFT_1_14, 0x3C));
        this.TO_CLIENT.register(CameraPacket.class, CameraPacket::new,
                map(Version.MINECRAFT_1_8, 0x43),
                map(Version.MINECRAFT_1_9, 0x36),
                map(Version.MINECRAFT_1_12, 0x38),
                map(Version.MINECRAFT_1_12_1, 0x39),
                map(Version.MINECRAFT_1_13, 0x3C),
                map(Version.MINECRAFT_1_14, 0x3E));
        this.TO_CLIENT.register(EntityAttachPacket.class, EntityAttachPacket::new,
                map(Version.MINECRAFT_1_8, 0x1B),
                map(Version.MINECRAFT_1_9, 0x3A),
                map(Version.MINECRAFT_1_12, 0x3C),
                map(Version.MINECRAFT_1_12_1, 0x3D),
                map(Version.MINECRAFT_1_13, 0x40),
                map(Version.MINECRAFT_1_14, 0x44));
        this.TO_CLIENT.register(SetPassengersPacket.class, SetPassengersPacket::new,
                map(Version.MINECRAFT_1_9, 0x40),
                map(Version.MINECRAFT_1_12, 0x42),
                map(Version.MINECRAFT_1_12_1, 0x43),
                map(Version.MINECRAFT_1_13, 0x46),
                map(Version.MINECRAFT_1_14, 0x4A));
        this.TO_CLIENT.register(CollectItemPacket.class, CollectItemPacket::new,
                map(Version.MINECRAFT_1_8, 0x0D),
                map(Version.MINECRAFT_1_9, 0x49),
                map(Version.MINECRAFT_1_10, 0x48),
                map(Version.MINECRAFT_1_12, 0x4A),
                map(Version.MINECRAFT_1_12_1, 0x4B),
                map(Version.MINECRAFT_1_13, 0x4F),
                map(Version.MINECRAFT_1_14, 0x55));
        this.TO_CLIENT.register(EntityEffectPacket.class, EntityEffectPacket::new,
                map(Version.MINECRAFT_1_8, 0x1D),
                map(Version.MINECRAFT_1_9, 0x4C),
                map(Version.MINECRAFT_1_12, 0x4E),
                map(Version.MINECRAFT_1_12_1, 0x4F),
                map(Version.MINECRAFT_1_13, 0x53),
                map(Version.MINECRAFT_1_14, 0x59));
        this.TO_CLIENT.register(UpdateEntityNBTPacket.class, UpdateEntityNBTPacket::new,
                map(Version.MINECRAFT_1_8, 0x49),
                map(Version.MINECRAFT_1_9, -1));
    }};

    private static Protocol[] byId = values();
    private static List<Protocol> protocols = Arrays.asList(byId);

    public Id2TypeVersionMapper<Packet> TO_SERVER = new Id2TypeVersionMapper<>();
    public Id2TypeVersionMapper<Packet> TO_CLIENT = new Id2TypeVersionMapper<>();

    Protocol() {
    }

    /**
     * Получить протокол по id
     */
    public static Protocol getById(int id) {
        return byId[id];
    }

    public static List<Protocol> getProtocols() {
        return protocols;
    }
}
