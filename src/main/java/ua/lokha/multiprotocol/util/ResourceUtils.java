package ua.lokha.multiprotocol.util;

import com.google.gson.Gson;
import ua.lokha.multiprotocol.Main;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

public class ResourceUtils {

    public static final Gson GSON = new Gson();

    public static InputStream getAsStream(String name) {
        return Main.getInstance().getResource(name);
    }

    public static BufferedReader getAsBufferedReader(String name) {
        InputStream resource = getAsStream(name);
        return resource != null ? new BufferedReader(new InputStreamReader(resource, StandardCharsets.UTF_8)) : null;
    }

}
