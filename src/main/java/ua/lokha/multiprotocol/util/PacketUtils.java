package ua.lokha.multiprotocol.util;

import com.google.common.base.Charsets;
import io.netty.buffer.ByteBuf;
import lombok.SneakyThrows;

import java.io.DataInput;
import java.io.UTFDataFormatException;
import java.nio.charset.StandardCharsets;
import java.util.*;

public class PacketUtils {

    public static String dump(ByteBuf buf) {
        buf.markReaderIndex();
        byte[] bytes = new byte[buf.readableBytes()];
        buf.readBytes(bytes);
        String result = Arrays.toString(bytes) + " - " + new String(bytes, StandardCharsets.UTF_8);
        buf.resetReaderIndex();
        return result;
    }

    public static void writeVarLong(long value, ByteBuf buf) {
        while((value & -128L) != 0L) {
            buf.writeByte((int)(value & 127L) | 128);
            value >>>= 7;
        }

        buf.writeByte((int)value);
    }

    public static long readVarLong(ByteBuf buf) {
        long i = 0L;
        int j = 0;

        byte b0;
        do {
            b0 = buf.readByte();
            i |= (long)(b0 & 127) << j++ * 7;
            if (j > 10) {
                throw new RuntimeException("VarLong too big");
            }
        } while((b0 & 128) == 128);

        return i;
    }

    public static void writeString(String s, ByteBuf buf) {
        if (s.length() > 32767) {
            throw new IllegalArgumentException(String.format("Cannot send string longer than Short.MAX_VALUE (got %s characters)", s.length()));
        } else {
            byte[] b = s.getBytes(Charsets.UTF_8);
            writeVarInt(b.length, buf);
            buf.writeBytes(b);
        }
    }

    /**
     * Прочитать строку используя формат {@link DataInput#readUTF()}
     */
    @SneakyThrows
    public static String readUTF(ByteBuf buf) {
        int utflen = buf.readUnsignedShort();
        byte[] bytearr = null;
        char[] chararr = null;
        bytearr = new byte[utflen];
        chararr = new char[utflen];

        int c, char2, char3;
        int count = 0;
        int chararr_count=0;

        buf.readBytes(bytearr, 0, utflen);

        while (count < utflen) {
            c = (int) bytearr[count] & 0xff;
            if (c > 127) break;
            count++;
            chararr[chararr_count++]=(char)c;
        }

        while (count < utflen) {
            c = (int) bytearr[count] & 0xff;
            switch (c >> 4) {
                case 0: case 1: case 2: case 3: case 4: case 5: case 6: case 7:
                    /* 0xxxxxxx*/
                    count++;
                    chararr[chararr_count++]=(char)c;
                    break;
                case 12: case 13:
                    /* 110x xxxx   10xx xxxx*/
                    count += 2;
                    if (count > utflen)
                        throw new UTFDataFormatException(
                                "malformed input: partial character at end");
                    char2 = (int) bytearr[count-1];
                    if ((char2 & 0xC0) != 0x80)
                        throw new UTFDataFormatException(
                                "malformed input around byte " + count);
                    chararr[chararr_count++]=(char)(((c & 0x1F) << 6) |
                            (char2 & 0x3F));
                    break;
                case 14:
                    /* 1110 xxxx  10xx xxxx  10xx xxxx */
                    count += 3;
                    if (count > utflen)
                        throw new UTFDataFormatException(
                                "malformed input: partial character at end");
                    char2 = (int) bytearr[count-2];
                    char3 = (int) bytearr[count-1];
                    if (((char2 & 0xC0) != 0x80) || ((char3 & 0xC0) != 0x80))
                        throw new UTFDataFormatException(
                                "malformed input around byte " + (count-1));
                    chararr[chararr_count++]=(char)(((c     & 0x0F) << 12) |
                            ((char2 & 0x3F) << 6)  |
                            ((char3 & 0x3F)));
                    break;
                default:
                    /* 10xx xxxx,  1111 xxxx */
                    throw new UTFDataFormatException(
                            "malformed input around byte " + count);
            }
        }
        // The number of chars produced may be less than utflen
        return new String(chararr, 0, chararr_count);
    }

    /**
     * Записать строку используя формат {@link java.io.DataOutput#writeUTF(String)}}
     */
    @SneakyThrows
    public static void writeUTF(String str, ByteBuf buf) {
        int strlen = str.length();
        int utflen = 0;
        int c, count = 0;

        /* use charAt instead of copying String to char array */
        for (int i = 0; i < strlen; i++) {
            c = str.charAt(i);
            if ((c >= 0x0001) && (c <= 0x007F)) {
                utflen++;
            } else if (c > 0x07FF) {
                utflen += 3;
            } else {
                utflen += 2;
            }
        }

        if (utflen > 65535)
            throw new UTFDataFormatException(
                    "encoded string too long: " + utflen + " bytes");

        byte[] bytearr = null;
        bytearr = new byte[utflen+2];

        bytearr[count++] = (byte) ((utflen >>> 8) & 0xFF);
        bytearr[count++] = (byte) ((utflen) & 0xFF);

        int i=0;
        for (i=0; i<strlen; i++) {
            c = str.charAt(i);
            if (!((c >= 0x0001) && (c <= 0x007F))) break;
            bytearr[count++] = (byte) c;
        }

        for (;i < strlen; i++){
            c = str.charAt(i);
            if ((c >= 0x0001) && (c <= 0x007F)) {
                bytearr[count++] = (byte) c;

            } else if (c > 0x07FF) {
                bytearr[count++] = (byte) (0xE0 | ((c >> 12) & 0x0F));
                bytearr[count++] = (byte) (0x80 | ((c >>  6) & 0x3F));
                bytearr[count++] = (byte) (0x80 | ((c) & 0x3F));
            } else {
                bytearr[count++] = (byte) (0xC0 | ((c >>  6) & 0x1F));
                bytearr[count++] = (byte) (0x80 | ((c) & 0x3F));
            }
        }
        buf.writeBytes(bytearr, 0, utflen+2);
//        return utflen + 2;
    }

    public static String readString(ByteBuf buf) {
        int len = readVarInt(buf);
        if (len > 32767) {
            throw new IllegalArgumentException(String.format("Cannot receive string longer than Short.MAX_VALUE (got %s characters)", len));
        } else {
            byte[] b = new byte[len];
            buf.readBytes(b);
            return new String(b, Charsets.UTF_8);
        }
    }

    public static void writeArray(byte[] b, ByteBuf buf) {
        if (b.length > 2097152) {
            throw new IllegalArgumentException(String.format("Cannot send byte array longer than 2097152 (got %s bytes)", b.length));
        } else {
            writeVarInt(b.length, buf);
            buf.writeBytes(b);
        }
    }

    public static byte[] toArray(ByteBuf buf) {
        byte[] ret = new byte[buf.readableBytes()];
        buf.readBytes(ret);
        return ret;
    }

    public static byte[] readArray(ByteBuf buf) {
        return readArray(buf, buf.readableBytes());
    }

    public static byte[] readArray(ByteBuf buf, int limit) {
        int len = readVarInt(buf);
        if (len > limit) {
            throw new IllegalArgumentException(String.format("Cannot receive byte array longer than %s (got %s bytes)", limit, len));
        } else {
            byte[] ret = new byte[len];
            buf.readBytes(ret);
            return ret;
        }
    }

    public static int[] readVarIntArray(ByteBuf buf) {
        int len = readVarInt(buf);
        int[] ret = new int[len];

        for(int i = 0; i < len; ++i) {
            ret[i] = readVarInt(buf);
        }

        return ret;
    }


    public static void writeVarIntArray(int[] array, ByteBuf buf) {
        writeVarInt(array.length, buf);
        for(int i = 0; i < array.length; ++i) {
            writeVarInt(array[i], buf);
        }
    }

    public static void writeStringArray(List<String> s, ByteBuf buf) {
        writeVarInt(s.size(), buf);
        Iterator var2 = s.iterator();

        while(var2.hasNext()) {
            String str = (String)var2.next();
            writeString(str, buf);
        }

    }

    public static List<String> readStringArray(ByteBuf buf) {
        int len = readVarInt(buf);
        List<String> ret = new ArrayList<>(len);

        for(int i = 0; i < len; ++i) {
            ret.add(readString(buf));
        }

        return ret;
    }

    public static int readVarInt(ByteBuf input) {
        return readVarInt(input, 5);
    }

    public static int readVarInt(ByteBuf input, int maxBytes) {
        int out = 0;
        int bytes = 0;

        byte in;
        do {
            in = input.readByte();
            out |= (in & 127) << bytes++ * 7;
            if (bytes > maxBytes) {
                throw new RuntimeException("VarInt too big");
            }
        } while((in & 128) == 128);

        return out;
    }

    public static void writeVarInt(int value, ByteBuf output) {
        do {
            int part = value & 127;
            value >>>= 7;
            if (value != 0) {
                part |= 128;
            }

            output.writeByte(part);
        } while(value != 0);

    }

    public static int readVarShort(ByteBuf buf) {
        int low = buf.readUnsignedShort();
        int high = 0;
        if ((low & '耀') != 0) {
            low &= 32767;
            high = buf.readUnsignedByte();
        }

        return (high & 255) << 15 | low;
    }

    public static void writeVarShort(ByteBuf buf, int toWrite) {
        int low = toWrite & 32767;
        int high = (toWrite & 8355840) >> 15;
        if (high != 0) {
            low |= 32768;
        }

        buf.writeShort(low);
        if (high != 0) {
            buf.writeByte(high);
        }

    }

    public static void writeUUID(UUID value, ByteBuf output) {
        output.writeLong(value.getMostSignificantBits());
        output.writeLong(value.getLeastSignificantBits());
    }

    public static UUID readUUID(ByteBuf input) {
        return new UUID(input.readLong(), input.readLong());
    }

}
