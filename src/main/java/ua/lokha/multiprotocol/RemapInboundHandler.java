package ua.lokha.multiprotocol;

import io.netty.buffer.ByteBuf;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import lombok.extern.java.Log;
import ua.lokha.multiprotocol.packet.play.*;

import java.util.Arrays;
import java.util.List;

import static ua.lokha.multiprotocol.util.PacketUtils.readVarInt;
import static ua.lokha.multiprotocol.util.PacketUtils.writeVarInt;

/**
 * Обработка входящих пакетов
 */
@ChannelHandler.Sharable
@Log
public class RemapInboundHandler extends ChannelInboundHandlerAdapter {

    private List<Class<? extends Packet>> ignoreLogging = Arrays.asList(
            ChunkDataPacket.class,
            EntityVelocityPacket.class,
            EntityTeleportPacket.class,
            TimeUpdatePacket.class,
            EntityLookAndRelativeMovePacket.class,
            EntityPacket.class,
            EntityLookPacket.class,
            EntityHeadLookPacket.class,
            EntityRelativeMovePacket.class,
            InPlayerPositionAndLookPacket.class,
            OutPlayerPositionAndLookPacket.class,
            PlayerPacket.class,
            PlayerLookPacket.class,
            PlayerPositionPacket.class
            );

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        if (!(msg instanceof ByteBuf) && !(msg instanceof Packet)) {
            super.channelRead(ctx, msg);
            return;
        }

        try {
            Channel channel = ctx.channel();

            Connection connection = Connection.get(channel);
            Protocol protocol = connection.getProtocol();
            Version version = connection.getVersion();

            Packet packet;
            int packetId;
            if (msg instanceof Packet) {
                packet = (Packet) msg;
                packetId = -2;
            } else {
                ByteBuf buf = (ByteBuf) msg;
                if (buf.readableBytes() == 0) {
                    return;
                }

                packetId = readVarInt(buf);
                try {
                    packet = protocol.TO_SERVER.create(packetId, version);
                } catch (NullPointerException e) {
                    packet = null;
                    log.severe("DEBUG: " + protocol + " " + packetId + " " + version);
                    e.printStackTrace();
                }

                if (packet == null) {
                    System.out.println("IN packet id not found " + packetId);
                    buf.release();
                    return;
                }

                packet.read(buf, version, connection);

                if (buf.readableBytes() > 0) {
                    log.warning("IN packet " + packetId + " buf.readableBytes() " + buf.readableBytes() + " > 0 " + packet);
                    buf.skipBytes(buf.readableBytes());
                }

                buf.release();
            }

            StringBuilder log = new StringBuilder("IN  packet " + packetId + " " + packet);

            List<Packet> remap = packet.map(Version.getServerVersion());
            for (Packet remapPacket : remap) {
                HandlerManager.getInstance().callHandlers(remapPacket, connection);

                int remapId = protocol.TO_SERVER.getId(remapPacket.getClass(), Version.getServerVersion());
                if (remapId != -1) {
                    ByteBuf outBuf = channel.alloc().buffer();
                    writeVarInt(remapId, outBuf);
                    remapPacket.write(outBuf, Version.getServerVersion(), connection);
                    ctx.fireChannelRead(outBuf);
                }

                log.append("\n     remap ").append(remapId).append(" ").append(remapPacket);
            }

            if (!ignoreLogging.contains(packet.getClass())) {
                System.out.println(log);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        log.severe("Ошибка обработки IN пакета для клиента " + Connection.get(ctx.channel()));
        cause.printStackTrace();
        super.exceptionCaught(ctx, cause);
    }
}
