package ua.lokha.multiprotocol.handler;

import ua.lokha.multiprotocol.Connection;
import ua.lokha.multiprotocol.PacketHandler;
import ua.lokha.multiprotocol.packet.play.SpawnMobPacket;

public class SpawnMobPacketHandler implements PacketHandler<SpawnMobPacket> {
    @Override
    public void handle(SpawnMobPacket packet, Connection connection) {
        connection.getViewEntities().put(packet.getEntityId(), packet.getEntityType());
    }
}
