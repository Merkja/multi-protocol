package ua.lokha.multiprotocol.handler;

import ua.lokha.multiprotocol.Connection;
import ua.lokha.multiprotocol.PacketHandler;
import ua.lokha.multiprotocol.packet.play.DestroyEntitiesPacket;

public class DestroyEntitiesPacketHandler implements PacketHandler<DestroyEntitiesPacket> {
    @Override
    public void handle(DestroyEntitiesPacket packet, Connection connection) {
        int[] entityIds = packet.getEntityIds();
        for (int i = 0; i < entityIds.length; i++) {
            connection.getViewEntities().remove(entityIds[i]);
        }
    }
}
