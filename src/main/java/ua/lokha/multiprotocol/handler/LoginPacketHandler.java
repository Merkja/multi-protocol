package ua.lokha.multiprotocol.handler;

import ua.lokha.multiprotocol.Connection;
import ua.lokha.multiprotocol.PacketHandler;
import ua.lokha.multiprotocol.packet.play.LoginPacket;
import ua.lokha.multiprotocol.type.EntityType;

/**
 * Получаем информацию о dimension
 */
public class LoginPacketHandler implements PacketHandler<LoginPacket> {
    @Override
    public void handle(LoginPacket packet, Connection connection) {
        connection.setDimension(packet.getDimension());
        connection.getViewEntities().put(packet.getEntityId(), EntityType.PLAYER);
    }
}
