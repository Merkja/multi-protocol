package ua.lokha.multiprotocol.handler;

import ua.lokha.multiprotocol.Connection;
import ua.lokha.multiprotocol.PacketHandler;
import ua.lokha.multiprotocol.Protocol;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.packet.handshake.HandshakePacket;

/**
 * Тут мы получаем информацию о версии и протоколе игрока
 */
public class HandshakePacketHandler implements PacketHandler<HandshakePacket> {

    @Override
    public void handle(HandshakePacket packet, Connection connection) {
        connection.setVersion(Version.getByProtocolVersion(packet.getProtocolVersion()));
        connection.setProtocol(Protocol.getById(packet.getRequestedProtocol()));
    }
}
