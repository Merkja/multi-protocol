package ua.lokha.multiprotocol;

public interface TypeCreator<T extends Type> {
    T create();
}
