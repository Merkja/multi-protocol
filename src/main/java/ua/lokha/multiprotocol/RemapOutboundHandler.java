package ua.lokha.multiprotocol;

import io.netty.buffer.ByteBuf;
import io.netty.channel.*;
import lombok.extern.java.Log;
import ua.lokha.multiprotocol.packet.play.*;

import java.util.Arrays;
import java.util.List;

import static ua.lokha.multiprotocol.util.PacketUtils.readVarInt;
import static ua.lokha.multiprotocol.util.PacketUtils.writeVarInt;

/**
 * Обработка входящих пакетов
 */
@ChannelHandler.Sharable
@Log
public class RemapOutboundHandler extends ChannelOutboundHandlerAdapter {

    private List<Class<? extends Packet>> ignoreLogging = Arrays.asList(
            ChunkDataPacket.class,
            EntityVelocityPacket.class,
            EntityTeleportPacket.class,
            TimeUpdatePacket.class,
            EntityLookAndRelativeMovePacket.class,
            EntityPacket.class,
            EntityLookPacket.class,
            EntityHeadLookPacket.class,
            EntityRelativeMovePacket.class,
            InPlayerPositionAndLookPacket.class,
            OutPlayerPositionAndLookPacket.class,
            PlayerPacket.class,
            PlayerLookPacket.class,
            PlayerPositionPacket.class
            );

    @Override
    public void write(ChannelHandlerContext ctx, Object msg, ChannelPromise promise) throws Exception {
        if (!(msg instanceof ByteBuf) && !(msg instanceof Packet)) {
            super.write(ctx, msg, promise);
            return;
        }

        try {
            Channel channel = ctx.channel();

            Connection connection = Connection.get(channel);
            Protocol protocol = connection.getProtocol();
            Version version = connection.getVersion();

            Packet packet;
            int packetId;
            if (msg instanceof Packet) {
                packet = (Packet) msg;
                packetId = -2;
            } else {
                ByteBuf buf = (ByteBuf) msg;
                if (buf.readableBytes() == 0) {
                    return;
                }

                packetId = readVarInt(buf);
                packet = protocol.TO_CLIENT.create(packetId, Version.getServerVersion());

                if (packet == null) {
                    System.out.println("OUT packet id not found " + packetId);
                    buf.release();
                    return;
                }

                packet.read(buf, Version.getServerVersion(), connection);

                if (buf.readableBytes() > 0) {
                    log.warning("OUT packet " + packetId + " buf.readableBytes() " + buf.readableBytes() + " > 0 " + packet);
                    buf.skipBytes(buf.readableBytes());
                }

                buf.release();
            }

            HandlerManager.getInstance().callHandlers(packet, connection);

            StringBuilder log = new StringBuilder("OUT packet " + packetId + " " + packet);

            List<Packet> remap = packet.map(version);
            for (Packet remapPacket : remap) {

                int remapId = protocol.TO_CLIENT.getId(remapPacket.getClass(), version);
                if (remapId != -1) {
                    ByteBuf outBuf = channel.alloc().buffer();
                    writeVarInt(remapId, outBuf);
                    remapPacket.write(outBuf, version, connection);
                    ctx.write(outBuf, promise);
                }

                log.append("\n     remap ").append(remapId).append(" ").append(remapPacket);
            }

            if (!ignoreLogging.contains(packet.getClass())) {
                System.out.println(log);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        log.severe("Ошибка обработки OUT пакета для клиента " + Connection.get(ctx.channel()));
        cause.printStackTrace();
        super.exceptionCaught(ctx, cause);
    }
}
