package ua.lokha.multiprotocol.type;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Type;
import ua.lokha.multiprotocol.Version;

@Data
public class Position implements Type {
    private int x;
    private int y;
    private int z;

    @Override
    public void read(ByteBuf buf, Version version) {
        long val = buf.readLong();
        x = (int) (val >> 38);
        y = (int) ((val >> 26) & 0xfff);
        z = (int) (val << 38 >> 38);
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        buf.writeLong((((long)x & 0x3FFFFFF) << 38) | (((long)y & 0xFFF) << 26) | (z & 0x3FFFFFF));
    }
}
