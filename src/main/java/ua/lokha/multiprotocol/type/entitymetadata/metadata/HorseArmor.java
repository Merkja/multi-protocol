package ua.lokha.multiprotocol.type.entitymetadata.metadata;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.type.VarIntType;
import ua.lokha.multiprotocol.type.entitymetadata.Metadata;
import ua.lokha.multiprotocol.type.entitymetadata.MetadataType;
import ua.lokha.multiprotocol.util.PacketUtils;

@Data
public class HorseArmor implements Metadata {
    private int armor;

    @Override
    public void read(ByteBuf buf, Version version) {
        armor = PacketUtils.readVarInt(buf);
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        PacketUtils.writeVarInt(armor, buf);
    }

    @Override
    public Class<? extends MetadataType> getTypeClass(Version version) {
        return VarIntType.class;
    }
}
