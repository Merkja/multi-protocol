package ua.lokha.multiprotocol.type.entitymetadata;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Type;
import ua.lokha.multiprotocol.Version;

/**
 * Типа данных позиции, который используется внутри метадаты entity
 */
@Data
public class Position implements Type, MetadataType {
    private int x;
    private int y;
    private int z;

    @Override
    public void read(ByteBuf buf, Version version) {
        if (version.isAfterOrEq(Version.MINECRAFT_1_9)) {
            long val = buf.readLong();
            x = (int) (val >> 38);
            y = (int) ((val >> 26) & 0xfff);
            z = (int) (val << 38 >> 38);
        } else {
            x = buf.readInt();
            y = buf.readInt();
            z = buf.readInt();
        }
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        if (version.isAfterOrEq(Version.MINECRAFT_1_9)) {
            buf.writeLong((((long)x & 0x3FFFFFF) << 38) | (((long)y & 0xFFF) << 26) | (z & 0x3FFFFFF));
        } else {
            buf.writeInt(x);
            buf.writeInt(y);
            buf.writeInt(z);
        }
    }
}
