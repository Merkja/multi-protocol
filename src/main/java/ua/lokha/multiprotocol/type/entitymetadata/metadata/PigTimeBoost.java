package ua.lokha.multiprotocol.type.entitymetadata.metadata;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.type.VarIntType;
import ua.lokha.multiprotocol.type.entitymetadata.Metadata;
import ua.lokha.multiprotocol.type.entitymetadata.MetadataType;
import ua.lokha.multiprotocol.util.PacketUtils;

@Data
public class PigTimeBoost implements Metadata {
    private int timeBoost;

    @Override
    public void read(ByteBuf buf, Version version) {
        timeBoost = PacketUtils.readVarInt(buf);
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        PacketUtils.writeVarInt(timeBoost, buf);
    }

    @Override
    public Class<? extends MetadataType> getTypeClass(Version version) {
        return VarIntType.class;
    }
}
