package ua.lokha.multiprotocol.type.entitymetadata.metadata;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.type.ShortType;
import ua.lokha.multiprotocol.type.VarIntType;
import ua.lokha.multiprotocol.type.entitymetadata.Metadata;
import ua.lokha.multiprotocol.type.entitymetadata.MetadataType;
import ua.lokha.multiprotocol.util.PacketUtils;

@Data
public class EntityAir implements Metadata {
    private int value;

    @Override
    public void read(ByteBuf buf, Version version) {
        if (version.isAfterOrEq(Version.MINECRAFT_1_9)) {
            value = PacketUtils.readVarInt(buf);
        } else {
            value = buf.readShort();
        }
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        if (version.isAfterOrEq(Version.MINECRAFT_1_9)) {
            PacketUtils.writeVarInt(value, buf);
        } else {
            buf.writeShort(value);
        }
    }

    @Override
    public Class<? extends MetadataType> getTypeClass(Version version) {
        if (version.isAfterOrEq(Version.MINECRAFT_1_9)) {
            return VarIntType.class;
        } else {
            return ShortType.class;
        }
    }
}
