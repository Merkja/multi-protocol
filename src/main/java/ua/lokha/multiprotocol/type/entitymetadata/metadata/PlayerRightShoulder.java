package ua.lokha.multiprotocol.type.entitymetadata.metadata;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.exception.UnsupportedVersionException;
import ua.lokha.multiprotocol.type.Nbt;
import ua.lokha.multiprotocol.type.entitymetadata.Metadata;
import ua.lokha.multiprotocol.type.entitymetadata.MetadataType;
import ua.lokha.multiprotocol.type.nbt.Tag;

@Data
public class PlayerRightShoulder implements Metadata {
    private Tag tag;

    @Override
    public void read(ByteBuf buf, Version version) {
        if (version.isBefore(Version.MINECRAFT_1_12)) {
            throw new UnsupportedVersionException(version);
        }
        tag = Tag.readNamedTag(buf);
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        if (version.isBefore(Version.MINECRAFT_1_12)) {
            throw new UnsupportedVersionException(version);
        }
        Tag.writeNamedTag(tag, buf);
    }

    @Override
    public Class<? extends MetadataType> getTypeClass(Version version) {
        if (version.isBefore(Version.MINECRAFT_1_12)) {
            throw new UnsupportedVersionException(version);
        }
        return Nbt.class;
    }
}
