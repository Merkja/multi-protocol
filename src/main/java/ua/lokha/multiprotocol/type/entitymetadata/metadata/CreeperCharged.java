package ua.lokha.multiprotocol.type.entitymetadata.metadata;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.type.BooleanType;
import ua.lokha.multiprotocol.type.entitymetadata.Metadata;
import ua.lokha.multiprotocol.type.entitymetadata.MetadataType;

@Data
public class CreeperCharged implements Metadata {
    private boolean charged;

    @Override
    public void read(ByteBuf buf, Version version) {
        charged = buf.readBoolean();
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        buf.writeBoolean(charged);
    }

    @Override
    public Class<? extends MetadataType> getTypeClass(Version version) {
        return BooleanType.class;
    }
}
