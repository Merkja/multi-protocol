package ua.lokha.multiprotocol.type;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import lombok.extern.java.Log;
import lombok.var;
import ua.lokha.multiprotocol.Id2EnumVersionMapper;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.util.ResourceUtils;

import java.util.Objects;

import static ua.lokha.multiprotocol.Id2EnumVersionMapper.map;

@Log
public class MaterialProtocol {
    public static Id2EnumVersionMapper<Material> BLOCK_PROTOCOL;
    public static Id2EnumVersionMapper<Material> ITEM_PROTOCOL;

    static {
        BLOCK_PROTOCOL = loadMaterialProtocol("block-protocol.json");
        ITEM_PROTOCOL = Id2EnumVersionMapper.loadFromResource("item-protocol.json", Material.class);
    }

    private static Id2EnumVersionMapper<Material> loadMaterialProtocol(String resourceName) {
        Id2EnumVersionMapper<Material> mapper = new Id2EnumVersionMapper<>(Material.class);

        JsonArray blocks = ResourceUtils.GSON.fromJson(Objects.requireNonNull(ResourceUtils.getAsBufferedReader(resourceName)), JsonArray.class);
        for (JsonElement element : blocks) {
            try {
                JsonObject block = element.getAsJsonObject();
                Material material = Material.valueOf(block.get("material").getAsString());

                JsonArray mapList = block.get("map").getAsJsonArray();
                var idMappers = new Id2EnumVersionMapper.IdMapper[mapList.size()];
                for (int i = 0; i < mapList.size(); i++) {
                    JsonObject mapItem = mapList.get(i).getAsJsonObject();
                    Version version = Version.valueOf(mapItem.get("version").getAsString());
                    int id = mapItem.get("id").getAsInt();
                    idMappers[i] = map(version, id);
                }

                mapper.register(material, idMappers);
            } catch (Exception e) {
                log.severe("Ошибка загрузки " + resourceName + ": " + element);
                e.printStackTrace();
            }
        }
        return mapper;
    }

    /**
     * Преобразовать указанный id блока в id в другой версии
     * @param id айди блока
     * @param version версия, в который был передан id
     * @param toVersion версия, в которую нужно преобразовать id
     * @return id этого же блока, только из другой версии, которая была передана аргументом toVersion
     */
    public static int mapBlockId(int id, Version version, Version toVersion) {
        if (id == 0) { // чаще всего проверяется, вынесего для оптимизации
            return 0;
        }
        Material material = MaterialProtocol.BLOCK_PROTOCOL.getEnum(id, version);
        if (material != null) {
            int newId = MaterialProtocol.BLOCK_PROTOCOL.getId(material, toVersion);
            if (newId != -1) {
                id = newId;
            } else {
                log.warning("remap id not found by material " + material + " and version " + toVersion);
                id = 0;
            }
        } else {
            log.warning("material not found by id " + id + " and version " + version);
            id = 0;
        }
        return id;
    }
}
