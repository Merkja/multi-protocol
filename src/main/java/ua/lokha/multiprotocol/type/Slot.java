package ua.lokha.multiprotocol.type;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import lombok.extern.java.Log;
import ua.lokha.multiprotocol.Type;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.type.entitymetadata.MetadataType;
import ua.lokha.multiprotocol.type.nbt.Tag;

import static ua.lokha.multiprotocol.util.PacketUtils.readVarInt;
import static ua.lokha.multiprotocol.util.PacketUtils.writeVarInt;

@Log
@Data
public class Slot implements Type, MetadataType {
    private Material material;
    private byte count;
    private short damage;
    private Tag tag;

    @Override
    public void read(ByteBuf buf, Version version) {
        if (version.isAfterOrEq(Version.MINECRAFT_1_13_2)) {
            boolean present = buf.readBoolean();
            if (present) {
                int id = readVarInt(buf);
                material = MaterialProtocol.ITEM_PROTOCOL.getEnum(id, version);
                if (material == null) {
                    log.warning("Material not found by id " + id + " and version " + version);
                    material = Material.STONE;
                }
                count = buf.readByte();
                tag = Tag.readNamedTag(buf);
            }
        } else {
            int id = buf.readShort();
            if (id != -1) {
                material = MaterialProtocol.ITEM_PROTOCOL.getEnum(id, version);
                if (material == null) {
                    log.warning("Material not found by id " + id + " and version " + version);
                    material = Material.STONE;
                }
                count = buf.readByte();
                if (version.isBeforeOrEq(Version.MINECRAFT_1_12_2)) {
                    damage = buf.readShort();
                }
                tag = Tag.readNamedTag(buf);
            }
        }
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        if (version.isAfterOrEq(Version.MINECRAFT_1_13_2)) {
            buf.writeBoolean(material != null);
            if (material != null) {
                int id = MaterialProtocol.ITEM_PROTOCOL.getId(material, version);
                if (id == -1) {
                    log.warning("id not found by Material " + material + " and version " + version);
                    id = 1;
                }
                writeVarInt(id, buf);
                buf.writeByte(count);
                Tag.writeNamedTag(tag, buf);
            }
        } else {
            if (material == null) {
                buf.writeShort(-1);
            } else {
                int id = MaterialProtocol.ITEM_PROTOCOL.getId(material, version);
                if (id == -1) {
                    log.warning("id not found by Material " + material + " and version " + version);
                    id = 1;
                }
                buf.writeShort(id);
                buf.writeByte(count);
                if (version.isBeforeOrEq(Version.MINECRAFT_1_12_2)) {
                    buf.writeShort(damage);
                }
                Tag.writeNamedTag(tag, buf);
            }
        }
    }
}
