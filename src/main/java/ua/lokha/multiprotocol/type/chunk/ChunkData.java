package ua.lokha.multiprotocol.type.chunk;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.exception.UnsupportedVersionException;
import ua.lokha.multiprotocol.type.MaterialProtocol;

import java.util.Arrays;

import static ua.lokha.multiprotocol.util.PacketUtils.readVarInt;
import static ua.lokha.multiprotocol.util.PacketUtils.writeVarInt;

@Data
public class ChunkData {
    /**
     * Формат чанков этой имплементации. Временная переменная
     */
    private static Version chunkFormatVersion = Version.MINECRAFT_1_8;

    private ChunkSection[] sections;
    private byte[] biomeData;

    public void read(ByteBuf buf, Version version, int dataLength, int primaryBitMask, boolean hasSkyLight) {
        if (version.isBefore(Version.MINECRAFT_1_9) || version.isAfter(Version.MINECRAFT_1_12_2)) {
            throw new UnsupportedVersionException(version);
        }
        int startIndex = buf.readerIndex();
        sections = new ChunkSection[16];
        for (int sectionY = 0; sectionY < sections.length; sectionY++) {
            if ((primaryBitMask & (1 << sectionY)) != 0) {
                ChunkSection section = new ChunkSection();

                section.setBitsPerBlock(buf.readUnsignedByte());
                int paletteLength = readVarInt(buf);
                if (paletteLength != 0) {
                    int[] palette = new int[paletteLength];
                    for (int i = 0; i < palette.length; i++) {
                        palette[i] = readVarInt(buf);
                    }
                    section.setPalette(palette);
                }
                long[] blockData = new long[readVarInt(buf)];
                for (int i = 0; i < blockData.length; i++) {
                    blockData[i] = buf.readLong();
                }
                section.setBlockData(blockData);
                byte[] blockLight = new byte[2048];
                buf.readBytes(blockLight);
                section.setBlockLight(blockLight);
                if (hasSkyLight) {
                    byte[] skyLight = new byte[2048];
                    buf.readBytes(skyLight);
                    section.setSkyLight(skyLight);
                } else {
                    section.setSkyLight(null);
                }

                sections[sectionY] = section;
            }
        }
        int bytesLeft = dataLength - (buf.readerIndex() - startIndex);
        if (bytesLeft == 256) {
            biomeData = new byte[256];
            buf.readBytes(biomeData);
        } else {
            biomeData = null;
        }
    }

    public void write(ByteBuf buf, Version version) {
        if (version.isAfterOrEq(Version.MINECRAFT_1_9)) { // для 1.9+
            for (int sectionY = 0; sectionY < sections.length; sectionY++) {
                ChunkSection section = sections[sectionY];
                if (section != null) {
                    buf.writeByte(section.getBitsPerBlock());

                    int[] palette = section.getPalette();
                    if (palette != null) {
                        writeVarInt(palette.length, buf);
                        if (version.isAfterOrEq(Version.MINECRAFT_1_13)) {
                            for (int i = 0; i < palette.length; i++) {
                                int id = palette[i];
                                id = MaterialProtocol.mapBlockId(id, chunkFormatVersion, version);
                                writeVarInt(id, buf);
                            }
                        } else {
                            for (int i = 0; i < palette.length; i++) {
                                writeVarInt(palette[i], buf);
                            }
                        }
                    } else if (version.isBeforeOrEq(Version.MINECRAFT_1_12_2)) {
                        writeVarInt(0, buf);
                    }

                    long[] blockData = section.getBlockData();
                    writeVarInt(blockData.length, buf);
                    for (int i = 0; i < blockData.length; i++) {
                        buf.writeLong(blockData[i]);
                    }

                    buf.writeBytes(section.getBlockLight());

                    if (section.getSkyLight() != null) {
                        buf.writeBytes(section.getSkyLight());
                    }
                }
            }
        } else { // remap 1.8, там формат другой
            for (int i = 0; i < sections.length; i++) {
                ChunkSection section = sections[i];
                if (section != null) {
                    for (int block = 0; block < 4096; block++) {
                        int blockState = section.getBlockState(block);
                        buf.writeByte((byte) blockState);
                        buf.writeByte((byte) (blockState >> 8));
                    }
                }
            }
            for (int i = 0; i < sections.length; i++) {
                ChunkSection section = sections[i];
                if (section != null) {
                    buf.writeBytes(section.getBlockLight());
                }
            }
            for (int i = 0; i < sections.length; i++) {
                ChunkSection section = sections[i];
                if (section != null && section.getSkyLight() != null) {
                    buf.writeBytes(section.getSkyLight());
                }
            }
        }
        if (biomeData != null) {
            if (version.isAfterOrEq(Version.MINECRAFT_1_13)) {
                for (int i = 0; i < biomeData.length; i++) {
                    buf.writeInt(biomeData[i] & 0xFF);
                }
            } else {
                buf.writeBytes(biomeData);
            }
        }
    }

    @Override
    public String toString() {
        return "ChunkData{" +
                "sections=" + Arrays.toString(sections) +
                ", biomeData.length=" + (biomeData == null ? null : biomeData.length) +
                '}';
    }
}
