package ua.lokha.multiprotocol.type;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Type;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.type.entitymetadata.MetadataType;

@Data
public class IntType implements Type, MetadataType {
    private int value;

    @Override
    public void read(ByteBuf buf, Version version) {
        value = buf.readInt();
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        buf.writeInt(value);
    }
}
