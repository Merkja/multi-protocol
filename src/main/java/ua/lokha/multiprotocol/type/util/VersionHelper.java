package ua.lokha.multiprotocol.type.util;

import ua.lokha.multiprotocol.Version;

public class VersionHelper {


    public static final Version[] ALL_1_12 = getAllBetween(Version.MINECRAFT_1_12, Version.MINECRAFT_1_12_2);

    public static final Version[] RANGE__1_11__1_12_2 = getAllBetween(Version.MINECRAFT_1_11, Version.MINECRAFT_1_12_2);

    public static final Version[] RANGE__1_9__1_12_2 = getAllBetween(Version.MINECRAFT_1_9, Version.MINECRAFT_1_12_2);

    public static final Version[] RANGE__1_10__1_12_2 = getAllBetween(Version.MINECRAFT_1_10, Version.MINECRAFT_1_12_2);


    public static Version[] getAllBetween(Version one, Version another) {
        int startId = Math.min(one.ordinal(), another.ordinal());
        int endId = Math.max(one.ordinal(), another.ordinal());
        Version[] between = new Version[(endId - startId) + 1];
        for (int i = startId; i <= endId; i++) {
            between[i - startId] = Version.getVersions().get(i);
        }
        return between;
    }

}
