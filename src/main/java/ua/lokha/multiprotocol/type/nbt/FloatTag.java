package ua.lokha.multiprotocol.type.nbt;

/**
 * Copyright Mojang AB.
 * 
 * Don't do evil.
 */

import io.netty.buffer.ByteBuf;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class FloatTag extends Tag {
    public float data;

    public FloatTag(String name) {
        super(name);
    }

    public FloatTag(String name, float data) {
        super(name);
        this.data = data;
    }

    @Override
    public void read(ByteBuf dis) {
        data = dis.readFloat();
    }

    @Override
    public void write(ByteBuf dos) {
        dos.writeFloat(data);
    }

    @Override
    public byte getId() {
        return TAG_Float;
    }

    @Override
    public Tag copy() {
        return new FloatTag(getName(), data);
    }
}
