package ua.lokha.multiprotocol.type.nbt;

/**
 * Copyright Mojang AB.
 * 
 * Don't do evil.
 */

import io.netty.buffer.ByteBuf;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class EndTag extends Tag {

    public EndTag() {
        super(null);
    }

    @Override
    public void read(ByteBuf dis) {
    }

    @Override
    public void write(ByteBuf dos) {
    }

    @Override
    public byte getId() {
        return TAG_End;
    }

    @Override
    public Tag copy() {
        return new EndTag();
    }
}
