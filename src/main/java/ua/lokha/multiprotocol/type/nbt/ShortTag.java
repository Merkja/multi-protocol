package ua.lokha.multiprotocol.type.nbt;

/**
 * Copyright Mojang AB.
 * 
 * Don't do evil.
 */

import io.netty.buffer.ByteBuf;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class ShortTag extends Tag {
    public short data;

    public ShortTag(String name) {
        super(name);
    }

    public ShortTag(String name, short data) {
        super(name);
        this.data = data;
    }

    @Override
    public void read(ByteBuf dis) {
        data = dis.readShort();
    }

    @Override
    public void write(ByteBuf dos) {
        dos.writeShort(data);
    }

    @Override
    public byte getId() {
        return TAG_Short;
    }

    @Override
    public Tag copy() {
        return new ShortTag(getName(), data);
    }
}
