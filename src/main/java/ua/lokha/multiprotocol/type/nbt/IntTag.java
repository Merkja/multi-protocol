package ua.lokha.multiprotocol.type.nbt;

/**
 * Copyright Mojang AB.
 * 
 * Don't do evil.
 */

import io.netty.buffer.ByteBuf;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class IntTag extends Tag {
    public int data;

    public IntTag(String name) {
        super(name);
    }

    public IntTag(String name, int data) {
        super(name);
        this.data = data;
    }

    @Override
    public void read(ByteBuf dis) {
        data = dis.readInt();
    }

    @Override
    public void write(ByteBuf dos) {
        dos.writeInt(data);
    }

    @Override
    public byte getId() {
        return TAG_Int;
    }

    @Override
    public Tag copy() {
        return new IntTag(getName(), data);
    }
}
