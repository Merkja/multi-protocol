package ua.lokha.multiprotocol.type;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Type;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.type.entitymetadata.MetadataType;
import ua.lokha.multiprotocol.util.PacketUtils;

@Data
public class OptChat implements Type, MetadataType {
    private boolean present;
    private String json;

    @Override
    public void read(ByteBuf buf, Version version) {
        present = buf.readBoolean();
        if (present) {
            json = PacketUtils.readString(buf);
        }
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        buf.writeBoolean(present);
        if (present) {
            PacketUtils.writeString(json, buf);
        }
    }
}
