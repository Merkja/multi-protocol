package ua.lokha.multiprotocol.type;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Type;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.type.entitymetadata.MetadataType;

@Data
public class OptPosition implements Type, MetadataType {
    private boolean present;
    private int x;
    private int y;
    private int z;

    @Override
    public void read(ByteBuf buf, Version version) {
        present = buf.readBoolean();
        if (present) {
            long val = buf.readLong();
            x = (int) (val >> 38);
            y = (int) ((val >> 26) & 0xfff);
            z = (int) (val << 38 >> 38);
        }
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        buf.writeBoolean(present);
        if (present) {
            buf.writeLong((((long)x & 0x3FFFFFF) << 38) | (((long)y & 0xFFF) << 26) | (z & 0x3FFFFFF));
        }
    }
}
