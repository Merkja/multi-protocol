package ua.lokha.multiprotocol;


/**
 * Базовый обработчик пакетов
 */
public interface PacketHandler<T extends Packet> {
    /**
     * Обработка пакета
     * @param packet пакет
     * @param connection информация о соединении игрока
     */
    void handle(T packet, Connection connection);
}
