package ua.lokha.multiprotocol.packet.login;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;

import static ua.lokha.multiprotocol.util.PacketUtils.readString;
import static ua.lokha.multiprotocol.util.PacketUtils.writeString;

@Data
public class LoginKickPacket implements Packet {
    private String message;

    @Override
    public void read(ByteBuf buf, Version version) {
        this.message = readString(buf);
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        writeString(this.message, buf);
    }
}
