package ua.lokha.multiprotocol.packet.login;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;

import static ua.lokha.multiprotocol.util.PacketUtils.readArray;
import static ua.lokha.multiprotocol.util.PacketUtils.writeArray;

@Data
public class EncryptionResponsePacket implements Packet {
    private byte[] sharedSecret;
    private byte[] verifyToken;

    @Override
    public void read(ByteBuf buf, Version version) {
        this.sharedSecret = readArray(buf, 128);
        this.verifyToken = readArray(buf, 128);
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        writeArray(this.sharedSecret, buf);
        writeArray(this.verifyToken, buf);
    }
}
