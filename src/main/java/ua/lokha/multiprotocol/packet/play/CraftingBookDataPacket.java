package ua.lokha.multiprotocol.packet.play;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.exception.UnsupportedVersionException;
import ua.lokha.multiprotocol.util.PacketUtils;

@Data
public class CraftingBookDataPacket implements Packet {

    private int type;
    private int recipeId;
    private boolean craftingRecipeBookOpen;
    private boolean craftingRecipeFilter;
    //1.13.1
    private boolean smeltingRecipeBookOpen;
    private boolean smeltingRecipeFilter;

    @Override
    public void read(ByteBuf buf, Version version) {
        if(version.isBefore(Version.MINECRAFT_1_12))
            throw new UnsupportedVersionException(version);

        type = PacketUtils.readVarInt(buf);

        if(version.isBeforeOrEq(Version.MINECRAFT_1_13)) {
            if(type == 0) {
                recipeId = buf.readInt();
            } else if(type == 1) {
                craftingRecipeBookOpen = buf.readBoolean();
                craftingRecipeFilter = buf.readBoolean();
            }
        } else if(version.isAfter(Version.MINECRAFT_1_13)) {
            if(type == 0) {
                recipeId = buf.readInt();
            } else if(type == 1) {
                craftingRecipeBookOpen = buf.readBoolean();
                craftingRecipeFilter = buf.readBoolean();
                smeltingRecipeBookOpen = buf.readBoolean();
                smeltingRecipeFilter = buf.readBoolean();
            }
        }
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        PacketUtils.writeVarInt(type, buf);

        if(type == 0) {
            buf.writeInt(recipeId);
        } else if(type == 1) {
            buf.writeBoolean(craftingRecipeBookOpen);
            buf.writeBoolean(craftingRecipeFilter);
        }
    }
}
