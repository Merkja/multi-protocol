package ua.lokha.multiprotocol.packet.play;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import lombok.extern.java.Log;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.type.Material;
import ua.lokha.multiprotocol.type.MaterialProtocol;

import static ua.lokha.multiprotocol.util.PacketUtils.readVarInt;
import static ua.lokha.multiprotocol.util.PacketUtils.writeVarInt;

@Data
@Log
public class BlockChangePacket implements Packet {

    private long position;
    public Material material;

    @Override
    public void read(ByteBuf buf, Version version) {
        position = buf.readLong();
        int id = readVarInt(buf);
        material = MaterialProtocol.BLOCK_PROTOCOL.getEnum(id, version);
        if (material == null) {
            log.warning("material not found by id " + id + " and version " + version);
            material = Material.AIR;
        }
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        buf.writeLong(position);
        int id = MaterialProtocol.BLOCK_PROTOCOL.getId(material, version);
        if (id == -1) {
            log.warning("id not found by material " + material + " and version " + version);
            id = 0;
        }
        writeVarInt(id, buf);
    }
}
