package ua.lokha.multiprotocol.packet.play;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;

@Data
public class OpenSignEditorPacket implements Packet {

    private long position;

    @Override
    public void read(ByteBuf buf, Version version) {
        position = buf.readLong();
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        buf.writeLong(position);
    }
}
