package ua.lokha.multiprotocol.packet.play;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import lombok.extern.java.Log;
import ua.lokha.multiprotocol.Connection;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.exception.UnsupportedVersionException;
import ua.lokha.multiprotocol.type.chunk.ChunkData;
import ua.lokha.multiprotocol.type.nbt.Tag;
import ua.lokha.multiprotocol.util.PacketUtils;

import static ua.lokha.multiprotocol.util.PacketUtils.readVarInt;
import static ua.lokha.multiprotocol.util.PacketUtils.writeVarInt;

// todo сделать remap
@Data
@Log
public class ChunkDataPacket implements Packet {

    /**
     * Тип мира, в котором находится игрок
     */
    private int dimension = -1;

    private int chunkX;
    private int chunkZ;
    private boolean groundUpContinuous;
    private int primaryBitMask;
    private ChunkData chunkData;
    private Tag[] tileEntities;

    public ChunkDataPacket() {
    }

    public ChunkDataPacket(int dimension) {
        this.dimension = dimension;
    }

    @Override
    public void read(ByteBuf buf, Version version, Connection connection) {
        this.dimension = connection.getDimension();
        this.read(buf, version);
    }

    @Override
    public void read(ByteBuf buf, Version version) {
        chunkX = buf.readInt();
        chunkZ = buf.readInt();
        groundUpContinuous = buf.readBoolean();
        if (version.isAfterOrEq(Version.MINECRAFT_1_9)) {
            primaryBitMask = readVarInt(buf);
        } else {
            primaryBitMask = buf.readUnsignedShort();
        }

        if (version.isBefore(Version.MINECRAFT_1_9) || version.isAfter(Version.MINECRAFT_1_12_2)) {
            throw new UnsupportedVersionException(version); // считывать умеем пока только чанки версии 1.9-1.12.2
        }

        boolean hasSkyLight;
        if (dimension == -1) {
            hasSkyLight = true; // наиболее вероятно
            log.warning("reading a ChunkDataPacket without specifying the dimension of world, " +
                    "please use the constructor ChunkDataPacket(int dimension)");
        } else {
            hasSkyLight = dimension == 0;
        }

        int dataLength = readVarInt(buf);
        chunkData = new ChunkData();
        chunkData.read(buf, version, dataLength, primaryBitMask, hasSkyLight);

        if (version.isAfterOrEq(Version.MINECRAFT_1_9_4)) {
            tileEntities = new Tag[readVarInt(buf)];
            for (int i = 0; i < tileEntities.length; i++) {
                tileEntities[i] = Tag.readNamedTag(buf);
            }
        }
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        buf.writeInt(chunkX);
        buf.writeInt(chunkZ);
        buf.writeBoolean(groundUpContinuous);
        if (version.isAfterOrEq(Version.MINECRAFT_1_9)) {
            writeVarInt(primaryBitMask, buf);
        } else {
            buf.writeShort(primaryBitMask);
        }

        if (chunkData != null) {
            ByteBuf chunkBuf = buf.alloc().buffer();
            chunkData.write(chunkBuf, version);
            writeVarInt(chunkBuf.readableBytes(), buf);
            buf.writeBytes(chunkBuf);
            chunkBuf.release();
        } else {
            PacketUtils.writeVarInt(0, buf);
        }

        if (version.isAfterOrEq(Version.MINECRAFT_1_9_4)) {
            writeVarInt(tileEntities.length, buf);
            for (int i = 0; i < tileEntities.length; i++) {
                Tag.writeNamedTag(tileEntities[i], buf);
            }
        }
    }
}
