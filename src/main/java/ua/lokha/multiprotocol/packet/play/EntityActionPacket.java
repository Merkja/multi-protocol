package ua.lokha.multiprotocol.packet.play;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;

import static ua.lokha.multiprotocol.util.PacketUtils.readVarInt;
import static ua.lokha.multiprotocol.util.PacketUtils.writeVarInt;

// todo сделать remap
// есть разница 1.8 и 1.9
// в 1.9 больше Action
// https://wiki.vg/index.php?title=Protocol&oldid=7368#Entity_Action
// https://wiki.vg/index.php?title=Protocol&oldid=7617#Entity_Action
@Data
public class EntityActionPacket implements Packet {
    private int entityId;
    private int actionId;
    private int jumpBoost;

    @Override
    public void read(ByteBuf buf, Version version) {
        entityId = readVarInt(buf);
        actionId = readVarInt(buf);
        jumpBoost = readVarInt(buf);
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        writeVarInt(entityId, buf);
        writeVarInt(actionId, buf);
        writeVarInt(jumpBoost, buf);
    }
}
