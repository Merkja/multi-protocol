package ua.lokha.multiprotocol.packet.play;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.RemapConventions;
import ua.lokha.multiprotocol.Version;

import java.util.UUID;

import static ua.lokha.multiprotocol.util.PacketUtils.*;

@Data
public class SpawnPaintingPacket implements Packet {
    private int entityId;
    private UUID uuid;
    private String title;
    private long position;
    private short direction;

    @Override
    public void read(ByteBuf buf, Version version) {
        entityId = readVarInt(buf);
        if (version.isAfterOrEq(Version.MINECRAFT_1_9)) {
            uuid = readUUID(buf);
        }
        title = readString(buf);
        position = buf.readLong();
        direction = buf.readUnsignedByte();
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        writeVarInt(entityId, buf);
        if (version.isAfterOrEq(Version.MINECRAFT_1_9)) {
            writeUUID(uuid != null ? uuid : RemapConventions.entityIdToUUID(entityId), buf);
        }
        writeString(title, buf);
        buf.writeLong(position);
        buf.writeByte(direction);
    }
}
