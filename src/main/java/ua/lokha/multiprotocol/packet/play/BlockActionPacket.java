package ua.lokha.multiprotocol.packet.play;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.util.PacketUtils;

@Data
public class BlockActionPacket implements Packet {

    private long position;
    private short actionId;
    private short actionParam;
    private int blockId;

    @Override
    public void read(ByteBuf buf, Version version) {
        position = buf.readLong();
        actionId = buf.readUnsignedByte();
        actionParam = buf.readUnsignedByte();
        blockId = PacketUtils.readVarInt(buf);
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        buf.writeLong(position);
        buf.writeByte(actionId);
        buf.writeByte(actionParam);
        PacketUtils.writeVarInt(blockId, buf);
    }
}
