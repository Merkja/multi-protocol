package ua.lokha.multiprotocol.packet.play;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.util.PacketUtils;

@Data
public class InAnimationPacket implements Packet {
    private int hand;

    @Override
    public void read(ByteBuf buf, Version version) {
        if (version.isAfterOrEq(Version.MINECRAFT_1_9)) {
            hand = PacketUtils.readVarInt(buf);
        }
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        if (version.isAfterOrEq(Version.MINECRAFT_1_9)) {
            PacketUtils.writeVarInt(hand, buf);
        }
    }
}
