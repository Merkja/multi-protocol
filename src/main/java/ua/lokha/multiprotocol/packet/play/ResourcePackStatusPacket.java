package ua.lokha.multiprotocol.packet.play;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.util.PacketUtils;

@Data
public class ResourcePackStatusPacket implements Packet {

    private String hash;
    //0: successfully loaded, 1: declined, 2: failed download, 3: accepted
    private int status;

    @Override
    public void read(ByteBuf buf, Version version) {
        if(version.isBefore(Version.MINECRAFT_1_10))
            hash = PacketUtils.readString(buf);
        status = PacketUtils.readVarInt(buf);
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        if(version.isBefore(Version.MINECRAFT_1_10))
            PacketUtils.writeString(hash, buf);
        PacketUtils.writeVarInt(status, buf);
    }
}
