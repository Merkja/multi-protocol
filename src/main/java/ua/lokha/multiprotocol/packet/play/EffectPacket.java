package ua.lokha.multiprotocol.packet.play;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;

@Data
public class EffectPacket implements Packet {
    private int effectId;
    private long position;
    private int data;
    private boolean disableRelativeVolume;

    @Override
    public void read(ByteBuf buf, Version version) {
        effectId = buf.readInt();
        position = buf.readLong();
        data = buf.readInt();
        disableRelativeVolume = buf.readBoolean();
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        buf.writeInt(effectId);
        buf.writeLong(position);
        buf.writeInt(data);
        buf.writeBoolean(disableRelativeVolume);
    }
}
