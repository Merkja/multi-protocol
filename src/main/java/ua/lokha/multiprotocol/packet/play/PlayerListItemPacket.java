package ua.lokha.multiprotocol.packet.play;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;

import java.util.UUID;

import static ua.lokha.multiprotocol.util.PacketUtils.*;

@Data
public class PlayerListItemPacket implements Packet {
    private Action action;
    private Item[] items;

    @Override
    public void read(ByteBuf buf, Version version) {
        action = Action.values()[readVarInt(buf)];
        items = new Item[readVarInt(buf)];
        for (int i = 0; i < items.length; i++) {
            Item item = items[i] = new Item();
            item.setUuid(readUUID(buf));
            switch (action) {
                case ADD_PLAYER:
                    item.username = readString(buf);
                    item.properties = new String[readVarInt(buf)][];
                    for (int j = 0; j < item.properties.length; j++) {
                        String name = readString(buf);
                        String value = readString(buf);
                        if (buf.readBoolean()) {
                            item.properties[j] = new String[]
                                    {
                                            name, value, readString(buf)
                                    };
                        } else {
                            item.properties[j] = new String[]
                                    {
                                            name, value
                                    };
                        }
                    }
                    item.gamemode = readVarInt(buf);
                    item.ping = readVarInt(buf);
                    if (buf.readBoolean()) {
                        item.displayName = readString(buf);
                    }
                    break;
                case UPDATE_GAMEMODE:
                    item.gamemode = readVarInt(buf);
                    break;
                case UPDATE_LATENCY:
                    item.ping = readVarInt(buf);
                    break;
                case UPDATE_DISPLAY_NAME:
                    if (buf.readBoolean()) {
                        item.displayName = readString(buf);
                    }
            }
        }
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        writeVarInt(action.ordinal(), buf);
        writeVarInt(items.length, buf);
        for (Item item : items) {
            writeUUID(item.uuid, buf);
            switch (action) {
                case ADD_PLAYER:
                    writeString(item.username, buf);
                    writeVarInt(item.properties.length, buf);
                    for (String[] prop : item.properties) {
                        writeString(prop[0], buf);
                        writeString(prop[1], buf);
                        if (prop.length >= 3) {
                            buf.writeBoolean(true);
                            writeString(prop[2], buf);
                        } else {
                            buf.writeBoolean(false);
                        }
                    }
                    writeVarInt(item.gamemode, buf);
                    writeVarInt(item.ping, buf);
                    buf.writeBoolean(item.displayName != null);
                    if (item.displayName != null) {
                        writeString(item.displayName, buf);
                    }
                    break;
                case UPDATE_GAMEMODE:
                    writeVarInt(item.gamemode, buf);
                    break;
                case UPDATE_LATENCY:
                    writeVarInt(item.ping, buf);
                    break;
                case UPDATE_DISPLAY_NAME:
                    buf.writeBoolean(item.displayName != null);
                    if (item.displayName != null) {
                        writeString(item.displayName, buf);
                    }
                    break;
            }
        }
    }

    public enum Action {

        ADD_PLAYER,
        UPDATE_GAMEMODE,
        UPDATE_LATENCY,
        UPDATE_DISPLAY_NAME,
        REMOVE_PLAYER
    }

    @Data
    public static class Item {
        // ALL
        private UUID uuid;

        // ADD_PLAYER
        private String username;
        private String[][] properties;

        // ADD_PLAYER & UPDATE_GAMEMODE
        private int gamemode;

        // ADD_PLAYER & UPDATE_LATENCY
        private int ping;

        // ADD_PLAYER & UPDATE_DISPLAY_NAME
        private String displayName;

    }
}
