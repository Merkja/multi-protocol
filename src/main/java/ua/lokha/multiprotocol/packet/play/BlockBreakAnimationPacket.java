package ua.lokha.multiprotocol.packet.play;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.util.PacketUtils;

@Data
public class BlockBreakAnimationPacket implements Packet {

    private int entityId;
    private long position;
    private byte destroyStage;

    @Override
    public void read(ByteBuf buf, Version version) {
        entityId = PacketUtils.readVarInt(buf);
        position = buf.readLong();
        destroyStage = buf.readByte();
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        PacketUtils.writeVarInt(entityId, buf);
        buf.writeLong(position);
        buf.writeByte(destroyStage);
    }
}
