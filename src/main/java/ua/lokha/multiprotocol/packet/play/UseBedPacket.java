package ua.lokha.multiprotocol.packet.play;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.exception.UnsupportedVersionException;
import ua.lokha.multiprotocol.util.PacketUtils;

@Data
public class UseBedPacket implements Packet {

    private int entityId;
    private long position;

    @Override
    public void read(ByteBuf buf, Version version) {
        if(version.isBeforeOrEq(Version.MINECRAFT_1_14))
            throw new UnsupportedVersionException(version);

        entityId = PacketUtils.readVarInt(buf);
        position = buf.readLong();
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        if(version.isBeforeOrEq(Version.MINECRAFT_1_14))
            throw new UnsupportedVersionException(version);

        PacketUtils.writeVarInt(entityId, buf);
        buf.writeLong(position);
    }
}
