package ua.lokha.multiprotocol.packet.play;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.exception.UnsupportedVersionException;
import ua.lokha.multiprotocol.util.PacketUtils;

@Data
public class CraftRecipeRequestPacket implements Packet {

    private byte windowId;
    private int recipeId;
    private boolean makeAll;

    @Override
    public void read(ByteBuf buf, Version version) {
        if(version.isBefore(Version.MINECRAFT_1_12_1))
            throw new UnsupportedVersionException(version);
        else {
            windowId = buf.readByte();
            recipeId = PacketUtils.readVarInt(buf);
            makeAll = buf.readBoolean();
        }
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        if(version.isBefore(Version.MINECRAFT_1_12_1))
            throw new UnsupportedVersionException(version);
        else {
            buf.writeByte(windowId);
            PacketUtils.writeVarInt(recipeId, buf);
            buf.writeBoolean(makeAll);
        }
    }
}
