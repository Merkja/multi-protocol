package ua.lokha.multiprotocol.packet.play;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.exception.UnsupportedVersionException;

//TODO: remap
@Data
public class SelectAdvancementTabPacket extends NotRemapPacket {

    @Override
    public void read(ByteBuf buf, Version version) {
        if(version.isBefore(Version.MINECRAFT_1_12))
            throw new UnsupportedVersionException(version);
        super.read(buf, version);
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        if(version.isBefore(Version.MINECRAFT_1_12))
            throw new UnsupportedVersionException(version);
        super.write(buf, version);
    }
}
