package ua.lokha.multiprotocol.packet.play;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;

import static ua.lokha.multiprotocol.util.PacketUtils.*;

@Data
public class ClientSettingsPacket implements Packet {

    private String locale;
    private byte viewDistance;
    private int chatFlags;
    private boolean chatColours;
    private byte difficulty;
    private byte skinParts;
    private int mainHand;

    @Override
    public void read(ByteBuf buf, Version version) {
        locale = readString( buf );
        viewDistance = buf.readByte();
        chatFlags = version.isAfterOrEq(Version.MINECRAFT_1_9) ? readVarInt( buf ) : buf.readUnsignedByte();
        chatColours = buf.readBoolean();
        skinParts = buf.readByte();
        if ( version.isAfterOrEq(Version.MINECRAFT_1_9) )
        {
            mainHand = readVarInt( buf );
        }
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        writeString( locale, buf );
        buf.writeByte( viewDistance );
        if ( version.isAfterOrEq(Version.MINECRAFT_1_9) )
        {
            writeVarInt( chatFlags, buf );
        } else
        {
            buf.writeByte( chatFlags );
        }
        buf.writeBoolean( chatColours );
        buf.writeByte( skinParts );
        if ( version.isAfterOrEq(Version.MINECRAFT_1_9) )
        {
            writeVarInt( mainHand, buf );
        }
    }
}
