package ua.lokha.multiprotocol.packet.play;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;

import static ua.lokha.multiprotocol.util.PacketUtils.*;

@Data
public class LoginPacket implements Packet {
    private int entityId;
    private short gameMode;
    private int dimension;
    private long seed;
    private short difficulty;
    private short maxPlayers;
    private String levelType;
    private int viewDistance;
    private boolean reducedDebugInfo;

    @Override
    public void read(ByteBuf buf, Version version) {
        entityId = buf.readInt();
        gameMode = buf.readUnsignedByte();
        if ( version.isAfter(Version.MINECRAFT_1_9) )
        {
            dimension = buf.readInt();
        } else
        {
            dimension = buf.readByte();
        }
        if ( version.isBefore(Version.MINECRAFT_1_14) )
        {
            difficulty = buf.readUnsignedByte();
        }
        maxPlayers = buf.readUnsignedByte();
        levelType = readString( buf );
        if ( version.isAfterOrEq(Version.MINECRAFT_1_14) )
        {
            viewDistance = readVarInt( buf );
        }
        if ( version.getProtocolVersion() >= 29 )
        {
            reducedDebugInfo = buf.readBoolean();
        }
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        buf.writeInt( entityId );
        buf.writeByte( gameMode );
        if ( version.isAfter(Version.MINECRAFT_1_9) )
        {
            buf.writeInt( dimension );
        } else
        {
            buf.writeByte( dimension );
        }
        if ( version.isBefore(Version.MINECRAFT_1_14) )
        {
            buf.writeByte( difficulty );
        }
        buf.writeByte( maxPlayers );
        writeString( levelType, buf );
        if ( version.isAfterOrEq(Version.MINECRAFT_1_14) )
        {
            writeVarInt( viewDistance, buf );
        }
        if ( version.getProtocolVersion() >= 29 )
        {
            buf.writeBoolean( reducedDebugInfo );
        }
    }
}
