package ua.lokha.multiprotocol.packet.play;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;

// todo сделать remap
// в новых версиях больше status'ов
@Data
public class EntityStatusPacket implements Packet {

    private int entityId;
    private byte status;

    @Override
    public void read(ByteBuf buf, Version version) {
        entityId = buf.readInt();
        status = buf.readByte();
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        buf.writeInt( entityId );
        buf.writeByte( status );
    }
}
