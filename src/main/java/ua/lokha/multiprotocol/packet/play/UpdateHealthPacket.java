package ua.lokha.multiprotocol.packet.play;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;

import static ua.lokha.multiprotocol.util.PacketUtils.readVarInt;
import static ua.lokha.multiprotocol.util.PacketUtils.writeVarInt;

@Data
public class UpdateHealthPacket implements Packet {
    public float health;
    public int food;
    public float foodSaturation;

    @Override
    public void read(ByteBuf buf, Version version) {
        health = buf.readFloat();
        food = readVarInt(buf);
        foodSaturation = buf.readFloat();
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        buf.writeFloat(health);
        writeVarInt(food, buf);
        buf.writeFloat(foodSaturation);
    }
}
