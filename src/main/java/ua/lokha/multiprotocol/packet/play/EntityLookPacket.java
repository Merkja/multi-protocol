package ua.lokha.multiprotocol.packet.play;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.util.PacketUtils;

@Data
public class EntityLookPacket implements Packet {
    private int entityId;
    private byte yaw;
    private byte pitch;
    private boolean onGround;

    @Override
    public void read(ByteBuf buf, Version version) {
        entityId = PacketUtils.readVarInt(buf);
        yaw = buf.readByte();
        pitch = buf.readByte();
        onGround = buf.readBoolean();
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        PacketUtils.writeVarInt(entityId, buf);
        buf.writeByte(yaw);
        buf.writeByte(pitch);
        buf.writeBoolean(onGround);
    }
}
