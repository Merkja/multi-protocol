package ua.lokha.multiprotocol.packet.play;

import com.google.common.collect.Maps;
import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.util.PacketUtils;

import java.util.Map;

//TODO: ремап для 1.13
@Data
public class StatisticsPacket implements Packet {

    private int statsCount;
    private Map<String, Integer> stats;

    @Override
    public void read(ByteBuf buf, Version version) {
        statsCount = PacketUtils.readVarInt(buf);
        stats = Maps.newHashMap();

        for(int i = 0; i < statsCount; i++) {
            stats.put(PacketUtils.readString(buf), PacketUtils.readVarInt(buf));
        }
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        PacketUtils.writeVarInt(statsCount, buf);

        stats.forEach((stat, count) -> {
            PacketUtils.writeString(stat, buf);
            PacketUtils.writeVarInt(count, buf);
        });
    }
}
