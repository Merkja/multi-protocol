package ua.lokha.multiprotocol.packet.play;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import lombok.Getter;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.util.PacketUtils;

@Data
public class OutAnimationPacket implements Packet {

    private int entityId;
    private AnimationType animationId;

    @Override
    public void read(ByteBuf buf, Version version) {
        entityId = PacketUtils.readVarInt(buf);
        animationId = AnimationType.getType(buf.readUnsignedByte(), version);
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        PacketUtils.writeVarInt(entityId, buf);

        // Замена анимации левой руки на правую
        if (animationId == AnimationType.SWING_OFFHAND) {
            buf.writeByte(0);
        } else {
            buf.writeByte(animationId.id);
        }
    }

    public enum AnimationType {
        SWING_MAIN_HAND((short) 0),
        TAKE_DAMAGE((short) 1),
        LEAVE_BED((short) 2),
        SWING_OFFHAND((short) 3), // Это на 1.9.1 и ниже заменяется на START_EATING
        CRITICAL_EFFECT((short) 4),
        MAGIC_CRITICAL_EFFECT((short) 5),
        START_EATING((short) 3);

        @Getter
        private short id;

        AnimationType(short id) {
            this.id = id;
        }

        private static OutAnimationPacket.AnimationType[] byOrdinal = values();

        public static OutAnimationPacket.AnimationType getType(short id, Version version) {
            if (id == 3 && version.isBefore(Version.MINECRAFT_1_9_1)) {
                return START_EATING;
            } else if (id <= 5) {
                return byOrdinal[id];
            }

            return null;
        }
    }
}
