package ua.lokha.multiprotocol.packet.play;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import net.minecraft.server.v1_12_R1.EnumParticle;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.exception.UnsupportedVersionException;
import ua.lokha.multiprotocol.util.PacketUtils;

import java.util.Collections;
import java.util.List;

//TODO: ремап под 1.13+
//В 1.13+ частица содержит больше данных
@Data
public class ParticlePacket implements Packet {

    private int particleId;
    private boolean longDistance;
    private float x, y, z;
    private float offX, offY, offZ;
    private float particleSpeed;
    private int particleCount;
    private int[] particleData;

    @Override
    public void read(ByteBuf buf, Version version) {
        if(version.isAfterOrEq(Version.MINECRAFT_1_13))
            throw new UnsupportedVersionException(version);

        particleId = buf.readInt();

        EnumParticle particle = EnumParticle.a(particleId);
        if(particle == null)
            particle = EnumParticle.BARRIER;

        longDistance = buf.readBoolean();

        x = buf.readFloat();
        y = buf.readFloat();
        z = buf.readFloat();

        offX = buf.readFloat();
        offY = buf.readFloat();
        offZ = buf.readFloat();

        particleSpeed = buf.readFloat();
        particleCount = buf.readInt();
        particleData = new int[particle.d()]; //getArgumentCount()

        for(int i = 0; i < particleData.length; i++) {
            particleData[i] = PacketUtils.readVarInt(buf);
        }

    }

    @Override
    public void write(ByteBuf buf, Version version) {
        if(version.isAfterOrEq(Version.MINECRAFT_1_13))
            throw new UnsupportedVersionException(version);

        buf.writeInt(particleId);

        buf.writeBoolean(longDistance);

        buf.writeFloat(x);
        buf.writeFloat(y);
        buf.writeFloat(z);

        buf.writeFloat(offX);
        buf.writeFloat(offY);
        buf.writeFloat(offZ);

        buf.writeFloat(particleSpeed);

        buf.writeInt(particleCount);

        for(int param: particleData) {
            PacketUtils.writeVarInt(param, buf);
        }
    }

    @Override
    public List<Packet> map(Version version) {
        if(particleId > 41 && version.isBefore(Version.MINECRAFT_1_9))
            return Collections.emptyList();

        return Collections.singletonList(this);
    }
}
