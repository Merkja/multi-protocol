package ua.lokha.multiprotocol.packet.play;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.type.nbt.Tag;

import java.util.Collections;
import java.util.List;

@Data
public class UpdateBlockEntityPacket implements Packet {

    private long position;
    private short action;
    private Tag tileCompound;

    @Override
    public void read(ByteBuf buf, Version version) {
        position = buf.readLong();
        action = buf.readUnsignedByte();
        tileCompound = Tag.readNamedTag(buf);
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        buf.writeLong(position);
        buf.writeByte(action);
        Tag.writeNamedTag(tileCompound, buf);
    }

    @Override
    public List<Packet> map(Version version) {
        if(version.isBefore(Version.MINECRAFT_1_9_2) && action > 6)
            return Collections.emptyList();
        else if(version.isBefore(Version.MINECRAFT_1_9_4) && action > 8)
            return Collections.emptyList();
        else if(version.isBefore(Version.MINECRAFT_1_11_1) && action > 9)
            return Collections.emptyList();
        else if(version.isBefore(Version.MINECRAFT_1_12) && action > 10)
            return Collections.emptyList();
        else if(version.isBefore(Version.MINECRAFT_1_14) && action > 11)
            return Collections.emptyList();

        return Collections.singletonList(this);
    }
}
