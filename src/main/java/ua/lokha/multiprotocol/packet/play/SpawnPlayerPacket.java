package ua.lokha.multiprotocol.packet.play;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.RemapConventions;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.type.entitymetadata.EntityMetadata;
import ua.lokha.multiprotocol.type.entitymetadata.MetadataProtocol;

import java.util.UUID;

import static ua.lokha.multiprotocol.util.PacketUtils.*;

@Data
public class SpawnPlayerPacket implements Packet {
    private int entityId;
    private UUID uuid;
    private double x;
    private double y;
    private double z;
    private byte yaw;
    private byte pitch;
    private short currentItem = 0;
    private EntityMetadata metadata;

    @Override
    public void read(ByteBuf buf, Version version) {
        entityId = readVarInt(buf);
        uuid = readUUID(buf);
        if (version.isAfterOrEq(Version.MINECRAFT_1_9)) {
            x = buf.readDouble();
            y = buf.readDouble();
            z = buf.readDouble();
        } else {
            x = buf.readInt() / 32.0D;
            y = buf.readInt() / 32.0D;
            z = buf.readInt() / 32.0D;
        }
        yaw = buf.readByte();
        pitch = buf.readByte();
        if (version.isBefore(Version.MINECRAFT_1_9)) {
            currentItem = buf.readShort();
        }
        metadata = new EntityMetadata(MetadataProtocol.PLAYER);
        metadata.read(buf, version);
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        writeVarInt(entityId, buf);
        writeUUID(uuid != null ? uuid : RemapConventions.entityIdToUUID(entityId), buf);
        if (version.isAfterOrEq(Version.MINECRAFT_1_9)) {
            buf.writeDouble(x);
            buf.writeDouble(y);
            buf.writeDouble(z);
        } else {
            buf.writeInt((int) (x * 32.0D));
            buf.writeInt((int) (y * 32.0D));
            buf.writeInt((int) (z * 32.0D));
        }
        buf.writeByte(yaw);
        buf.writeByte(pitch);
        if (version.isBefore(Version.MINECRAFT_1_9)) {
            buf.writeShort(currentItem);
        }
        if (metadata != null) {
            metadata.write(buf, version);
        } else {
            buf.writeByte(EntityMetadata.getEndIndex(version));
        }
    }
}
