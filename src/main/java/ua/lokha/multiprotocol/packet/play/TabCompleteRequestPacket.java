package ua.lokha.multiprotocol.packet.play;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;

import static ua.lokha.multiprotocol.util.PacketUtils.*;

@Data
public class TabCompleteRequestPacket implements Packet {

    private int transactionId;
    private String cursor;
    private boolean assumeCommand;
    private boolean hasPositon;
    private long position;


    @Override
    public void read(ByteBuf buf, Version version) {
        if ( version.isAfterOrEq(Version.MINECRAFT_1_13) )
        {
            transactionId = readVarInt( buf );
        }
        cursor = readString( buf );

        if ( version.isBefore(Version.MINECRAFT_1_13) )
        {
            if ( version.isAfterOrEq(Version.MINECRAFT_1_9) )
            {
                assumeCommand = buf.readBoolean();
            }

            if ( hasPositon = buf.readBoolean() )
            {
                position = buf.readLong();
            }
        }
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        if ( version.isAfterOrEq(Version.MINECRAFT_1_13) )
        {
            writeVarInt( transactionId, buf );
        }
        writeString( cursor, buf );

        if ( version.isBefore(Version.MINECRAFT_1_13) )
        {
            if ( version.isAfterOrEq(Version.MINECRAFT_1_9) )
            {
                buf.writeBoolean( assumeCommand );
            }

            buf.writeBoolean( hasPositon );
            if ( hasPositon )
            {
                buf.writeLong( position );
            }
        }
    }
}
