package ua.lokha.multiprotocol.packet.play;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;

@Data
public class OutConfirmTransactionPacket implements Packet {

    private byte windowId;
    private short actionId;
    private boolean accepted;

    @Override
    public void read(ByteBuf buf, Version version) {
        windowId = buf.readByte();
        actionId = buf.readShort();
        accepted = buf.readBoolean();
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        buf.writeByte(windowId);
        buf.writeShort(actionId);
        buf.writeBoolean(accepted);
    }
}
