package ua.lokha.multiprotocol.packet.play;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.util.PacketUtils;

import java.util.UUID;

@Data
public class SpectatePacket implements Packet {

    private UUID uuid;

    @Override
    public void read(ByteBuf buf, Version version) {
        uuid = PacketUtils.readUUID(buf);
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        PacketUtils.writeUUID(uuid, buf);
    }
}
